<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        // $data = [
        //     0 => ['id' => '1', 'name' => 'ผู้บริหาร'],
        //     1 => ['id' => '2', 'name' => 'ฝ่ายบริหารงานทั่วไป'],
        //     2 => ['id' => '3', 'name' => 'ฝ่ายอบรมและประสานงาน'],
        //     3 => ['id' => '4', 'name' => 'ฝ่ายกิจกรรมอาสากาชาด'],
        //     4 => ['id' => '5', 'name' => 'ฝ่ายประชาสัมพันธ์'],
        //     5 => ['id' => '6', 'name' => 'กลุ่มงานแผนงานและพัฒนา']
        // ];
        // DB::table('group_users')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_users');
    }
}
