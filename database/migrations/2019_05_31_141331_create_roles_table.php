<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('label');
            $table->text('description')->nullable();
            $table->timestamps();
        });

        // $data = [
        //     0 => ['id' => '1', 'name' => 'administrator', 'label' => 'ผู้ดูแลระบบ'],
        //     1 => ['id' => '2', 'name' => 'officer', 'label' => 'เจ้าหน้าที่ระบบ'],
        //     2 => ['id' => '3', 'name' => 'user', 'label' => 'เจ้าหน้าที่ทั่วไป'],
        // ];
        // DB::table('roles')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
