<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('password');
            $table->integer('titles_id');
            $table->string('fullname');
            $table->string('email');
            $table->integer('group_users_id');
            $table->tinyInteger('roles_id');
            $table->integer('active');
            $table->rememberToken();
            $table->timestamps();
        });

        $data = [
            'username' => 'adminvb',
            'password' => Hash::make('123456'),
            'titles_id' => '1',
            'fullname' => 'Administrator',
            'email' => 'admin@local.com',
            'group_users_id' => '1',
            'active' => '1',
            'roles_id' => '1'
        ];
        DB::table('users')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
