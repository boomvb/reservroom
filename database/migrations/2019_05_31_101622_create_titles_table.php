<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('titles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        // $data = [
        //     0 => ['id' => '1', 'name' => 'นาย'],
        //     1 => ['id' => '2', 'name' => 'นางสาว'],
        //     2 => ['id' => '3', 'name' => 'นาง'],
        //     3 => ['id' => '4', 'name' => 'นายแพทย์']
        // ];
        // DB::table('titles')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('titles');
    }
}
