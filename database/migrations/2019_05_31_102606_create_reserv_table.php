<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservs', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->date('startday');
            $table->date('endday');
            $table->time('starttime');
            $table->time('endtime');
            $table->integer('rooms_id');
            $table->integer('types_id');
            $table->string('topic');
            $table->text('desc')->nullable();
            $table->integer('num')->nullable();
            $table->text('namejoin')->nullable();
            $table->string('tel')->nullable();
            $table->integer('check_catering')->nullable();
            $table->text('txt_catering2')->nullable();
            $table->text('txt_cateringother')->nullable();
            $table->integer('check_projector')->nullable();
            $table->integer('check_screen')->nullable();
            $table->integer('check_dvd')->nullable();
            $table->integer('check_tv')->nullable();
            $table->integer('check_record')->nullable();
            $table->integer('check_amp')->nullable();
            $table->integer('check_control')->nullable();
            $table->text('txt_control')->nullable();
            $table->integer('check_wireless_mic')->nullable();
            $table->string('txt_wireless_mic')->nullable();
            $table->integer('check_mic')->nullable();
            $table->string('txt_mic')->nullable();
            $table->integer('check_other')->nullable();
            $table->string('txt_other')->nullable();
            $table->integer('status_reservs_id');
            $table->dateTime('create_date')->nullable();
            $table->integer('position_reservs_id');
            $table->integer('user_id');
            $table->text('note')->nullable();
            $table->text('note_position_reserv')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservs');
    }
}
