<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Auth::routes();

Route::group(['middleware' => 'auth'],function(){

    //Reservs//
    Route::group(['prefix' => 'reservs', 'as' => 'reserv.'], function () {
        Route::get('/view/{slug}', 'ReservController@index')->name('view');
        Route::get('/create', 'ReservController@create')->name('create');
        Route::post('/save', 'ReservController@store')->name('save');
        Route::get('/show/{id}', 'ReservController@show')->name('show');
        Route::get('/edit/{id}', 'ReservController@edit')->name('edit');
        Route::post('/update', 'ReservController@update')->name('update');
        Route::get('/export/{id}', 'ReservController@exportPDF')->name('export');
        Route::post('/updateStatus', 'ReservController@updateStatus')->name('updateStatus');
        Route::post('/updateStatusAll', 'ReservController@updateStatusAll')->name('updateStatusAll');
        Route::post('/note', 'ReservController@storeNote')->name('note');

        //Calendar//
        Route::get('/events', 'ReservController@GetEvents')->name('events');

        //ห้องประชุมย่อยจาก Rooms//
        Route::post('/childrenroom', 'ReservController@GetChildrenRooms')->name('childrenroom');
    });

    //Rooms//
    Route::group(['prefix' => 'rooms', 'as' => 'room.'], function () {
        Route::get('/view', 'RoomController@index')->name('view');
        Route::get('/create', 'RoomController@create')->name('create');
        Route::post('/save', 'RoomController@store')->name('save');
        Route::get('/edit/{id}', 'RoomController@edit')->name('edit');
        Route::post('/update', 'RoomController@update')->name('update');
        Route::get('/remove/{id}', 'RoomController@destroy')->name('remove');
    });

    //Reports//
    Route::group(['prefix' => 'reports', 'as' => 'report.'], function () {
        Route::get('/view', 'ReportController@index')->name('view');
        Route::post('/report_year', 'ReportController@report_year')->name('reportyear');
        Route::post('/report_month', 'ReportController@report_month')->name('reportmonth');
        Route::post('/report_mannul', 'ReportController@report_mannul')->name('reportmannul');
    });

    //Users//
    Route::group(['prefix' => 'users', 'as' => 'user.'], function () {
        Route::get('/view', 'UserController@index')->name('view');
        Route::get('/create', 'UserController@create')->name('create');
        Route::post('/save', 'UserController@store')->name('save');
        Route::get('/edit/{id}', 'UserController@edit')->name('edit');
        Route::post('/update/{id}', 'UserController@update')->name('update');
        Route::post('/active', 'UserController@active')->name('active');
        Route::get('/history/{id}', 'UserController@history')->name('history');
        Route::get('/editprofile/{id}', 'UserController@editProfile')->name('editprofile');
        Route::post('/updateprofile/{id}', 'UserController@updateProfile')->name('updateprofile');
        Route::get('/setpassword/{id}', 'UserController@setPassword')->name('setpassword');
        Route::post('/active', 'UserController@enabled')->name('active');
        Route::post('/inactive', 'UserController@disabled')->name('inactive');
    });

    //Configs//
    Route::group(['prefix' => 'set_basics', 'as' => 'set_basic.'], function () {
        Route::get('/view', 'Basic\SetBasicController@index')->name('view');

        //GroupUser//
        Route::group(['prefix' => 'groupusers', 'as' => 'groupuser.'], function () {
            Route::get('/view', 'Basic\GroupUserController@index')->name('view');
            Route::get('/create', 'Basic\GroupUserController@create')->name('create');
            Route::post('/save', 'Basic\GroupUserController@store')->name('save');
            Route::get('/edit/{id}', 'Basic\GroupUserController@edit')->name('edit');
            Route::post('/update/{id}', 'Basic\GroupUserController@update')->name('update');
        });

        //Roles//
        Route::group(['prefix' => 'roles', 'as' => 'role.'], function () {
            Route::get('/view', 'Basic\RolesController@index')->name('view');
            Route::get('/create', 'Basic\RolesController@create')->name('create');
            Route::post('/save', 'Basic\RolesController@store')->name('save');
            Route::get('/edit/{id}', 'Basic\RolesController@edit')->name('edit');
            Route::post('/update/{id}', 'Basic\RolesController@update')->name('update');
        });


    });

});
