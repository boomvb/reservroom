<script src="{{ asset('public/vendor/sweetalert/sweetalert.all.js') }}"></script>
@if (Session::has('error'))
    <script>
        Swal.fire({!! Session::pull('alert.config') !!});
    </script>
@endif
