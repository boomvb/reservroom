@extends('layouts.layout')

@section('title', 'ระบบจองห้องประชุมสำนักงานอาสากาชาด')

@section('content')

@include('layouts.inc-menu')
@include('layouts.inc-minicontent-top')

<div class="col-md-12">
    <div class="page-header">
        <h1>สร้างบัญชีผู้ใช้งาน</h1>
    </div>

	<!--MessageAlert-->
    @include('flash_msg')
    <!--MessageAlert-->

    <div class="col-md-6 col-md-offset-3">

    <div class="panel panel-danger">
        <div class="panel-heading"><i class="fa fa-user-o" aria-hidden="true"></i> สร้างบัญชีผู้ใช้งาน</div>
        <div class="panel-body">
            <form action="{{ route('user.save') }}" method="post" class="form-horizontal">
            {{ csrf_field() }}
            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                <label for="title" class="col-sm-2 control-label">คำนำหน้า</label>
                <div class="col-sm-4">
                    <select class="form-control" name="title" id="title">
                        <option value="">--- กรุณาเลือก ---</option>
                        @foreach($titles as $title)
                        <option value="{{ $title->id }}">{{ $title->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-sm-2 control-label">ชื่อ-นามสกุล</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                </div>
            </div>
            <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                <label for="username" class="col-sm-2 control-label">ชื่อผู้ใช้งาน</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                </div>
            </div>
            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-sm-2 control-label">รหัสผ่าน</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-sm-2 control-label">อีเมล์</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                </div>
            </div>
            <div class="form-group {{ $errors->has('group_user') ? ' has-error' : '' }}">
                <label for="group_user" class="col-sm-2 control-label">ฝ่าย</label>
                <div class="col-sm-8">
                    <select class="form-control" name="group_user" id="group_user">
                        <option value="">--- กรุณาเลือกฝ่าย ---</option>
                        @foreach($groupusers as $groupuser)
                        <option value="{{ $groupuser->id }}">{{ $groupuser->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group {{ $errors->has('role') ? ' has-error' : '' }}">
                <label for="role" class="col-sm-2 control-label">สิทธิ์</label>
                <div class="col-sm-6">
                    <select class="form-control" id="role" name="role">
                    <option value="">--- กรุณาเลือก ---</option>
                    @foreach($roles as $role)
                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                    @endforeach
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-10">
                    <button type="submit" class="btn btn-success" id="btn_save_user"><span class="glyphicon glyphicon-floppy-disk"></span> สร้าง</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>


@include('layouts.inc-minicontent-bottom')
@endsection