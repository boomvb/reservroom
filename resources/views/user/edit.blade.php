@extends('layouts.layout')

@section('title', 'ระบบจองห้องประชุมสำนักงานอาสากาชาด')

@section('content')

@include('layouts.inc-menu')
@include('layouts.inc-minicontent-top')

<div class="col-lg-12">
    <div class="page-header">
        <h1>แก้ไขข้อมูลผู้ใช้งาน</h1>
    </div>

	<!--MessageAlert-->
	@include('flash_msg')
    <!--MessageAlert-->

    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-success">
            <div class="panel-heading"><span class="glyphicon glyphicon-pencil"></span>แก้ไขข้อมูลผู้ใช้งาน</div>
        <div class="panel-body">
        <form class="form-horizontal" method="POST" action="{{ route('user.update',$edituser->id) }}">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="edit_title" class="col-sm-2 control-label">คำนำหน้า </label>
                    <div class="col-sm-5">
                        <select class="form-control" name="title" id="title">
                        <option value="">--- กรุณาเลือก ---</option>
                        @foreach($titles as $title)
                            <option value="{{ $title->id }}" @if($edituser->titles_id == $title->id) selected="selected" @endif >{{ $title->name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_name_log" class="col-sm-2 control-label">ชื่อ-นามสกุล</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" value="{{ $edituser->fullname }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_username_log" class="col-sm-2 control-label">ชื่อผู้ใช้งาน</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="username" name="username" value="{{ $edituser->username }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_email_log" class="col-sm-2 control-label">อีเมล์</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="email" name="email" value="{{ $edituser->email }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_group_user" class="col-sm-2 control-label">ฝ่าย </label>
                    <div class="col-sm-5">
                        <select class="form-control" name="group_user" id="group_user">
                        <option value="">--- กรุณาเลือกฝ่าย ---</option>
                        @foreach($groupusers as $groupUser)
                            <option value="{{ $groupUser->id }}" @if($edituser->group_users_id == $groupUser->id) selected="selected" @endif >{{ $groupUser->name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">   
                    <label for="edit_role" class="col-sm-2 control-label">สิทธิ์</label>
                    <div class="col-sm-4">     
                        <select class="form-control" id="role" name="role">
                        <option value="">--- กรุณาเลือก ---</option>
                        @foreach($roles as $role)
                            <option value="{{ $role->id }}" @if($edituser->roles_id == $role->id) selected="selected" @endif >{{ $role->name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-10">
                        <input type="hidden" id="id_user" name="id_user" value="{{ $edituser->id }}">
                        <button type="submit" class="btn btn-success" id="btn_edit_member"><span class="glyphicon glyphicon-floppy-disk"></span> แก้ไขข้อมูล</button>
                    </div>
                </div>
                </form>
        </div>
        </div>

    </div>

</div>

@include('layouts.inc-minicontent-bottom')
@endsection