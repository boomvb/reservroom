@extends('layouts.layout')

@section('title', 'ระบบจองห้องประชุมสำนักงานอาสากาชาด')

@section('content')

@include('layouts.inc-menu')
@include('layouts.inc-minicontent-top')
<div class="col-md-12">
    <div class="page-header">
        <h1>แก้ไขข้อมูล</h1>
    </div>

	<!--MessageAlert-->
	@include('flash_msg')
    <!--MessageAlert-->
    
    <div id="user-profile-3" class="user-profile row">
    <div class="col-sm-offset-1 col-sm-10">
        <form class="form-horizontal" action="{{ route('user.updateprofile', $profiles->id) }}" method="post">
        {{ csrf_field() }}
            <div class="tabbable">
                <ul class="nav nav-tabs padding-16">
                    <li class="active">
                        <a data-toggle="tab" href="#edit-basic">
                            <i class="green ace-icon fa fa-pencil-square-o bigger-125"></i>
                            ข้อมูลส่วนตัว
                        </a>
                    </li>
                </ul>

                <div class="tab-content profile-edit-tab-content">
                    <div id="edit-basic" class="tab-pane in active">
                        <h4 class="header blue bolder smaller">ข้อมูลทั่วไป</h4>

                        <div class="row">

                            <div class="col-xs-12 col-sm-8">
                                <div class="form-group">
                                    <label for="title" class="col-sm-4 control-label no-padding-right">คำนำหน้า </label>
                                    <div class="col-sm-4">
                                        <select class="form-control" name="title" id="title">
                                        <option value="">--- กรุณาเลือก ---</option>
                                        @foreach($titles as $title)
                                            <option value="{{ $title->id }}" @if($profiles->titles_id == $title->id) selected @endif>{{ $title->name }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="name">ชื่อ-นามสกุล</label>

                                    <div class="col-sm-5">
                                            <input class="form-control" type="text" id="name" name="name" placeholder="First Name" value="{{ $profiles->fullname }}" />
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-4 control-label no-padding-right" for="username">ชื่อผู้ใช้งาน</label>

                                    <div class="col-sm-6">
                                        
                                        <input class="form-control" type="text" id="username" name="username" placeholder="Username" value="{{ $profiles->username }}" @if(Auth::user()->roles_id != 1) disabled @endif>
                                    </div>
                                </div>

                                <div class="space-4"></div>
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="email">อีเมล์</label>

                                    <div class="col-sm-6">
                                        <span class="input-icon input-icon-right">
                                            <input class="form-control" type="text" id="email" name="email" placeholder="xxx@email.com" value="{{ $profiles->email }}" />
                                            <i class="ace-icon fa fa-envelope"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="groupuser">ฝ่าย</label>

                                    <div class="col-sm-5">
                                        <select class="form-control" id="groupuser" name="groupuser">
                                            <option value="">--- กรุณาเลือก ---</option>
                                            @foreach($groupusers as $groupuser)
                                            <option value="{{ $groupuser->id }}" @if($profiles->group_users_id == $groupuser->id) selected @endif @if(Auth::user()->roles_id != 1) disabled @endif>{{ $groupuser->name }}</option>
                                            @endforeach                                            
                                        </select>
                                    </div>
                                </div>
                        <div class="space"></div>

                        <h4 class="header blue bolder smaller">เปลี่ยนรหัสผ่าน</h4>

                        <div class="form-group">
                            <label class="col-sm-4 control-label no-padding-right" for="new_password">รหัสผ่านใหม่</label>

                            <div class="col-sm-4">
                                <input class="form-control" type="password" id="new_password" name="new_password" />
                            </div>
                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label no-padding-right" for="confirm_password">ยืนยันรหัสผ่าน</label>

                            <div class="col-sm-4">
                                <input class="form-control" type="password" id="confirm_password" name="confirm_password" />
                            </div>
                        </div>
                    </div>
                    </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="uid_user" value="">
            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit" id="btn_edit_user">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        บันทึก
                    </button>

                </div>
            </div>
        </form>
    </div><!-- /.span -->
</div><!-- /.user-profile -->




@include('layouts.inc-minicontent-bottom')
@endsection