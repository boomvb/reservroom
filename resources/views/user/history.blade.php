@extends('layouts.layout')

@section('title', 'ระบบจองห้องประชุมสำนักงานอาสากาชาด')

@section('content')

@include('layouts.inc-menu')
@include('layouts.inc-minicontent-top')
<div class="col-md-12">
    <div class="page-header">
        <h1><i class="fa fa-history" aria-hidden="true"></i>&nbsp;ประวัติการจอง</h1>
    </div>

    <table class="table table-striped table-bordered" cellspacing="0">
        <tr>
            <th width="2%">เลขที่จอง</th>
            <th width="5%">สถานะ</th>
            <th width="15%">หัวข้อประชุม</th>
            <th width="15%">ห้องประชุม</th>
            <th width="10%">วันที่-เวลาจอง</th>
            <th width="10%">วันที่-เวลาสิ้นสุด</th>
            <th width="8%" align="center">&nbsp;</th>
        </tr>
        @foreach($history as $key => $his)
        <tr>
            <td>{{ $his->id }}</td>
            <td><font color="@if($his->status->id == 1) #A0522D @elseif($his->status->id == 2) #5cb85c @elseif($his->status->id == 3) #d9534f @endif">
                    @if($his->status->id == 1)
                        <i class="fa fa-circle-o-notch" aria-hidden="true"></i>
                    @elseif($his->status->id == 2)
                        <i class="fa fa-check-square" aria-hidden="true"></i>
                    @elseif($his->status->id == 3)
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    @endif
                    {{ $his->status->name }}</font></td>
            <td>{{ $his->topic }}</td>
            <td>{{ $his->room->name }}</td>
            <td>{{ ConvertDate($his->startday) }} <br> {{ ConvertTime($his->starttime) }}&nbsp;น.</td>
            <td>{{ ConvertDate($his->endday) }} <br> {{ ConvertTime($his->endtime) }}&nbsp;น.</td>
            <td><center><button class="btn btn-app btn-info btn-xs" onclick="javascript:window.location.href='{{ route('reserv.show',$his->id) }}'">
            <i class="ace-icon fa fa-info bigger-120"></i>ดูข้อมูล</button></center></td>
        </tr>
        @endforeach
    </table>
    <p>ทั้งหมด {{ $history->total() }} รายการ</p>
    {{ $history->links() }}

@include('layouts.inc-minicontent-bottom')
@endsection