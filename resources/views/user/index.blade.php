@extends('layouts.layout')

@section('title', 'ระบบจองห้องประชุมสำนักงานอาสากาชาด')

@section('content')

@include('layouts.inc-menu')
@include('layouts.inc-minicontent-top')

<div class="row">
    <div class="col-xs-12">

        <div class="page-header">
			<h1>รายชื่อผู้ใช้งาน</h1>
        </div>

		<!--MessageAlert-->
		@include('flash_msg')
        <!--MessageAlert-->
        

        <table class="table table-bordered">
            <tr>
                <th width="2%">ลำดับที่</th>
                <th width="13%">ชื่อ-นามสกุล</th>
                <th width="8%">ชื่อผู้ใช้งาน</th>
                <th width="8%">ฝ่าย</th>
                <th width="5%">สิทธิ์ผู้ใช้งาน</th>
                <th width="20%">&nbsp;</th>
            </tr>
            @foreach($users as $row => $user)
            <tr>
                <td>{{ $users->firstItem() + $row }}</td>
                <td>{{ $user->titles->name }} {{ $user->fullname }}</td>
                <td>{{ $user->username }}</td>
                <td>{{ $user->group->name }}</td>
                <td>{{ $user->role->name }}</td>
                @php
                    $icon_button;
                    $text_button;
                    $color_button;
                    $function_button;
                    if($user->active == 1){
                        $icon_button = "lock";
                        $text_button = "ปิดการใช้งาน";
                        $color_button = "danger";
                        $function_button = "inactiveUser";
                    }elseif($user->active == 0){
                        $icon_button = "unlock";
                        $text_button = "เปิดการใช้งาน";
                        $color_button = "success";
                        $function_button = "activeUser";                    
                    }
                @endphp                
                <td>
                    <a class="btn btn-info" href="{{ route('user.edit', $user->id) }}" role="button"><i class="fa fa-pencil fa-fw"></i>&nbsp;แก้ไขข้อมูล</a>
                    <button class="btn btn-{{ $color_button }}" href="#" onclick="{{ $function_button }}('{{ $user->id }}')" role="button"><i class="fa fa-{{ $icon_button }} fa-fw"></i>{{ $text_button }}</button>
                    <a class="btn btn-warning" href="{{ route('user.setpassword', $user->id) }}" onclick="" role="button">ตั้งรหัสผ่านใหม่ 123456</a>
                </td>
            </tr>
            @endforeach
        </table>
        <p>ทั้งหมด {{ $users->total() }} รายการ</p>
        {{ $users->links() }}
    </div>
</div>

@include('layouts.inc-minicontent-bottom')
@endsection
@section('scripts')
    <script>
        function activeUser(id){
            $.ajax({
                type: "POST",
                url: "{{ route('user.inactive') }}",
                data: {
                    id : id,
                    _token : "{{ csrf_token() }}"
                },
                success: function(data){
                    if(data.status == true){
                        swal("เปิดการใช้งานเรียบร้อยแล้ว","","success")
                            .then(function(){ 
                            window.location.reload();
                        });
                    }
                }
            });
        }
        function inactiveUser(id){
            $.ajax({
                type: "POST",
                url: "{{ route('user.active') }}",
                data: {
                    id : id,
                    _token : "{{ csrf_token() }}"
                },
                success: function(data){
                    if(data.status == true){
                        swal("ปิดการใช้งานเรียบร้อยแล้ว","","success")
                            .then(function(){ 
                            window.location.reload();
                        });                        
                    }
                }
            });
        }
    </script>

@endsection