@extends('layouts.layout')

@section('title', 'ระบบจองห้องประชุมสำนักงานอาสากาชาด')

@section('content')

@include('layouts.inc-menu')
@include('layouts.inc-minicontent-top')
<div class="row">

    <div class="col-xs-12">
        <div class="page-header">
            <h3>ตั้งค่าระบบ</h3>
        </div>

        <div class="col-md-6 col-md-offset-3">
        <table class="table table-bordered">
            <tr>
                <td align="center"><a href=""><i class="fa fa-handshake-o fa-4x" aria-hidden="true"></i><br><br> ตั้งค่าฝ่าย</a></td>
                <td align="center"><a href=""><i class="fa fa-database fa-4x" aria-hidden="true"></i><br><br> สำรองข้อมูล</a></td>
            </tr>
            <tr>
                <td align="center"><a href=""><i class="fa fa-bolt fa-4x" aria-hidden="true"></i><br><br> สถานะห้อง</a></td>
                <td align="center"><a href="{{ route('set_basic.role.view') }}"><i class="fa fa-user fa-4x" aria-hidden="true"></i><br><br> ตั้งค่าสิทธิ์</a></td>
            </tr>
        </table>
        </div>
    </div>

</div>

@include('layouts.inc-minicontent-bottom')
@endsection