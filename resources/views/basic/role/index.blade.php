@extends('layouts.layout')

@section('title', 'ระบบจองห้องประชุมสำนักงานอาสากาชาด')

@section('content')

@include('layouts.inc-menu')
@include('layouts.inc-minicontent-top')
<div class="row">
    <div class="col-xs-12">
        <div class="page-header">
            <h3>สิทธิ์ผู้ใช้งาน</h3>
        </div>

		<!--MessageAlert-->
		@include('flash_msg')
        <!--MessageAlert-->

        <div class="pull-right inline">
            <a href="{{ route('set_basic.role.create') }}" class="btn btn-app btn-primary btn-sm"><i class="ace-icon fa fa-pencil-square-o bigger-230"></i>เพิ่ม<span class="badge badge-warning badge-left"></span></a>            
        </div>
        <table class="table table-striped">
            <tr>
                <th>ลำดับที่</th>
                <th>รายการ</th>
                <th>เข้าถึงการใช้งาน</th>
            </tr>
            @foreach($roles as $key => $role)
            <tr>
                <td>{{ $roles->firstItem() + $key }}</td>
                <td>{{ $role->name }}</td>
                <td>---</td>
            </tr>
            @endforeach
        </table>

</div>

@include('layouts.inc-minicontent-bottom')
@endsection