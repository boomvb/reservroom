@extends('layouts.layout')

@section('title', 'ระบบจองห้องประชุมสำนักงานอาสากาชาด')

@section('content')

@include('layouts.inc-menu')
@include('layouts.inc-minicontent-top')

<div class="col-md-12">
    <div class="page-header">
        <h1>สร้างสิทธิ์ผู้ใช้งาน</h1>
    </div>
    <!--MessageAlert-->
    @include('flash_msg')
    <!--MessageAlert-->
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-danger">
            <div class="panel-heading"><i class="fa fa-user-o" aria-hidden="true"></i> สร้างสิทธิ์ผู้ใช้งาน</div>

            <div class="panel-body">
                <form action="{{ route('set_basic.role.save') }}" method="post" class="form-horizontal">
                {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="title" class="col-sm-2 control-label">ชื่อสิทธิ์ผู้ใช้งาน</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="role" class="col-sm-2 control-label">สิทธิ์</label>
                        <div class="col-sm-6">
							<div class="checkbox">
								<label>
									<input name="permission[]" type="checkbox" class="ace" value="reserv">
									<span class="lbl"> จองห้องประชุม</span>
								</label>
                            </div>
							<div class="checkbox">
								<label>
									<input name="permission[]" type="checkbox" class="ace" value="room">
									<span class="lbl"> ห้องประชุม</span>
								</label>
                            </div>
							<div class="checkbox">
								<label>
									<input name="permission[]" type="checkbox" class="ace" value="report">
									<span class="lbl"> รายงาน</span>
								</label>
                            </div>
							<div class="checkbox">
								<label>
									<input name="permission[]" type="checkbox" class="ace" value="user">
									<span class="lbl"> บัญชีผู้ใช้งาน</span>
								</label>
                            </div>
							<div class="checkbox">
								<label>
									<input name="roles[]" type="checkbox" class="ace" value="basic">
									<span class="lbl"> ตั้งค่าระบบ</span>
								</label>
							</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-5 col-sm-10">
                            <button type="submit" class="btn btn-success" id="btn_save_user"><span class="glyphicon glyphicon-floppy-disk"></span> สร้าง</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>



@include('layouts.inc-minicontent-bottom')
@endsection
