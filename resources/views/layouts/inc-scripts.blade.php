<script src="{{asset('js/jquery.js') }}"></script>
<script src="{{asset('js/bootbox.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('js/jquery-ui.js')}}"></script>
<script src="{{asset('js/moment.min.js')}}"></script>
<script src="{{asset('js/fullcalendar.js')}}"></script>
<script src="{{asset('js/th.js')}}"></script>
<!--Sweetalert -->
<script src="{{ asset('js/sweetalert.min.js') }}"></script>
<!-- ace scripts -->
<script src="{{asset('js/elements.scroller.js')}}"></script>
<script src="{{asset('js/elements.colorpicker.js')}}"></script>
<script src="{{asset('js/elements.fileinput.js')}}"></script>
<script src="{{asset('js/elements.typeahead.js')}}"></script>
<script src="{{asset('js/elements.wysiwyg.js')}}"></script>
<script src="{{asset('js/elements.spinner.js')}}"></script>
<script src="{{asset('js/elements.treeview.js')}}"></script>
<script src="{{asset('js/elements.wizard.js')}}"></script>
<script src="{{asset('js/elements.aside.js')}}"></script>
<script src="{{asset('js/ace.js')}}"></script>
<script src="{{asset('js/ace.ajax-content.js')}}"></script>
<script src="{{asset('js/ace.touch-drag.js')}}"></script>
<script src="{{asset('js/ace.sidebar.js')}}"></script>
<script src="{{asset('js/ace.sidebar-scroll-1.js')}}"></script>
<script src="{{asset('js/ace.submenu-hover.js')}}"></script>
<script src="{{asset('js/ace.widget-box.js')}}"></script>
<script src="{{asset('js/ace.settings.js')}}"></script>
<script src="{{asset('js/ace.settings-rtl.js')}}"></script>
<script src="{{asset('js/ace.settings-skin.js')}}"></script>
<script src="{{asset('js/ace.widget-on-reload.js')}}"></script>
<script src="{{asset('js/ace.searchbox-autocomplete.js')}}"></script>
@stack('scripts')
