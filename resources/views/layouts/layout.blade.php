<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>@yield('title')</title>
@include('layouts.inc-stylesheets')
@yield('stylesheets')
</head>
<body class="no-skin">
    <!-- Add Content-->
    @yield('content')
    <!-- Add Content-->

@include('layouts.inc-scripts')
@yield('scripts')
</body>
</html>