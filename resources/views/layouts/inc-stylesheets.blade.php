<!-- bootstrap -->
<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" />
<!-- text fonts -->
<link rel="stylesheet" href="{{ asset('css/jquery-ui.custom.css') }}" />
<!-- text fonts -->
<link rel="stylesheet" href="{{ asset('css/ace-fonts.css') }}" />
<!-- fontsawesome -->
<link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}"/>
<!-- Fullcalendar -->
<link rel="stylesheet" href="{{ asset('css/fullcalendar.css') }}">
<!-- ace styles -->
<link rel="stylesheet" href="{{ asset('css/ace.css') }}" class="ace-main-stylesheet" id="main-ace-style" />
<!-- inline styles related to this page -->
<!-- select2 -->
<link rel="stylesheet" href="{{ asset('css/select2.css') }}">
<link rel="stylesheet" href="{{ asset('css/select2-bootstrap.css') }}">
<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

<!--[if lte IE 8]>
<script src="../assets/js/html5shiv.js"></script>
<script src="../assets/js/respond.js"></script>
<![endif]-->
<!-- custom styles -->
<link rel="stylesheet" href="{{ asset('css/style.css') }}" class="ace-main-stylesheet" id="main-ace-style" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">

@stack('stylesheets')