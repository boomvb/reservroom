<!-- Top Menu -->
<div id="navbar" class="navbar navbar-default">
    <div class="navbar-container" id="navbar-container">
        <!-- #section:basics/sidebar.mobile.toggle -->
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only">Toggle sidebar</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>
        </button>
        <script type="text/javascript">
            try{ace.settings.check('navbar' , 'fixed')}catch(e){}
        </script>
        <!-- /section:basics/sidebar.mobile.toggle -->
        <div class="navbar-header pull-left">
            <!-- #section:basics/navbar.layout.brand -->
            <a href="#" class="navbar-brand">
                <small>
                    <i class="fa fa-leaf"></i>
                    ระบบจองห้องประชุม สำนักงานอาสากาชาด สภากาชาดไทย
                </small>
            </a>


            <!-- /section:basics/navbar.toggle -->
        </div>
        <!-- #section:basics/navbar.dropdown -->
        <div class="navbar-buttons navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">
                <li class="purple">

                <!-- #section:basics/navbar.user_menu -->
                <li class="light-blue">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <span class="user-info">
                            <small>ยินดีต้อนรับ,</small>
                            {{ Auth::user()->titles->name }} {{ Auth::user()->fullname }} <br>
                            ({{ Auth::user()->group->name }})
                        </span>

                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>

                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <li>
                            <a href="{{ route('user.history', Auth::user()->id) }}">
                                <i class="ace-icon fa fa-history"></i>
                                ประวัติการจอง 
                                @php $count = App\Reserv::where('user_id', Auth::user()->id)->count(); @endphp
                                <span class="badge">{{ $count }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('user.editprofile', Auth::user()->id) }}">
                                <i class="ace-icon fa fa-user"></i>
                                แก้ไขข้อมูล
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="ace-icon fa fa-power-off"></i>
                                ออกจากระบบ
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>                            
                        </li>
                    </ul>
                </li>

                <!-- /section:basics/navbar.user_menu -->
            </ul>
        </div>
    </div><!-- /.navbar-container -->
</div>

<!---------------------------------------------------------------------------------------->

<!--Left Menu -->
<div class="main-container" id="main-container">
    <script type="text/javascript">
        try{ace.settings.check('main-container' , 'fixed')}catch(e){}
    </script>
        
	<script type="text/javascript">
			try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
	</script>

	  <div class="sidebar-shortcuts" id="sidebar-shortcuts">

    </div><!-- /.sidebar-shortcuts -->

    <!--Menu-->
        <!-- #section:basics/sidebar -->
        <div id="sidebar" class="sidebar responsive">
            <ul class="nav nav-list">
              @if(checkPermission(['user','officer','administrator']))
                  <li class="{{ Request::is( 'reservs/view/today', 'reservs/view/calendar', 'reservs/view/table', 'reservs/create', 'reservs/report') ? 'open' : '' }}">
                    <a href="#" class="dropdown-toggle">
                      <i class="menu-icon fa fa-bandcamp"></i>
                      <span class="menu-text"> 1.การจองห้อง </span>

                      <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                      <li class="{{ Request::is( 'reservs/view/today' , 'reservs/view/calendar', 'reservs/view/table') ? 'active' : '' }}">
                        <a href="{{ url('reservs/view/today') }}">
                          <i class="menu-icon fa fa-caret-right"></i>
                          รายการการใช้ห้องประชุม
                        </a>

                        <b class="arrow"></b>
                      </li>

                      <li class="{{ Request::is( 'reservs/create') ? 'active' : '' }}">
                        <a href="{{ url('reservs/create') }}">
                          <i class="menu-icon fa fa-caret-right"></i>
                          จองห้องประชุม
                        </a>

                        <b class="arrow"></b>
                      </li>
                      <li class="{{ Request::is( 'reservs/report') ? 'active' : '' }}">
                        <a href="#">
                          <i class="menu-icon fa fa-caret-right"></i>
                          พิมพ์รายการการใช้ห้องประชุม
                        </a>
                      </li>
                    </ul>
                </li>
              @endif
              @if(checkPermission(['officer','administrator']))
                <li class="{{ Request::is( 'rooms/view', 'rooms/create') ? 'open' : '' }}">
                  <a href="#" class="dropdown-toggle">
                    <span class="menu-icon glyphicon glyphicon-flag"></span>
                    <span class="menu-text"> 2.ห้องประชุม </span>

                    <b class="arrow fa fa-angle-down"></b>
                  </a>

                  <b class="arrow"></b>

                  <ul class="submenu">
                    <li class="{{ Request::is( 'rooms/view') ? 'active' : '' }}">
                      <a href="{{ url('rooms/view') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        รายชื่อห้องประชุม
                      </a>

                      <b class="arrow"></b>
                    </li>

                    <li class="{{ Request::is( 'rooms/create') ? 'active' : '' }}">
                      <a href="{{ url('rooms/create') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        สร้างห้องประชุม
                      </a>

                      <b class="arrow"></b>
                    </li>
                  </ul>
                </li>
                <li class="{{ Request::is( 'reports/view') ? 'active' : '' }}">
                  <a href="{{ route('report.view') }}">
                    <i class="menu-icon fa fa-file-text"></i>
                    <span class="menu-text"> 3.รายงาน </span>
                  </a>
                  <b class="arrow"></b>
                </li>
              @endif
              @if(checkPermission(['administrator']))
                <li class="{{ Request::is( 'users/view', 'users/create') ? 'open' : '' }}">
                  <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-user-circle-o"></i>
                    <span class="menu-text"> 4.บัญชีผู้ใช้งาน </span>

                    <b class="arrow fa fa-angle-down"></b>
                  </a>

                  <b class="arrow"></b>

                  <ul class="submenu">
                    <li class="{{ Request::is( 'users/view') ? 'active' : '' }}">
                      <a href="{{ url('users/view') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        รายชื่อบัญชีผู้ใช้งาน
                      </a>

                      <b class="arrow"></b>
                    </li>

                    <li class="{{ Request::is( 'users/create') ? 'active' : '' }}">
                      <a href="{{ url('users/create') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        สร้างบัญชีผู้ใช้งาน
                      </a>

                      <b class="arrow"></b>
                    </li>
                  </ul>
                </li>
                <li class="{{ Request::is( 'set_basics/view') ? 'active' : '' }}">
                  <a href="{{ route('set_basic.view') }}">
                    <i class="menu-icon fa fa-cog"></i>
                    <span class="menu-text"> 5.ตั้งค่าระบบ </span>
                  </a>
    
                  <b class="arrow"></b>
                </li>
              @endif
            </ul><!-- /.nav-list -->

            <!-- #section:basics/sidebar.layout.minimize -->
            <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
            </div>

            <!-- /section:basics/sidebar.layout.minimize -->
            <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
            </script>
        </div>
