@extends('layouts.layout')

@section('title', 'ระบบจองห้องประชุมสำนักงานอาสากาชาด')

@section('content')

@include('layouts.inc-menu')
@include('layouts.inc-minicontent-top')

<div class="row">
	<div class="col-xs-12">
        <div class="page-header">
            <h1>สร้างห้องประชุม</h1>
        </div>
        <!--MessageAlert-->
        @include('flash_msg')
        <!--MessageAlert-->
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading">สร้างห้องประชุม</div>
                <br>
                    <form class="form-horizontal" action="{{ route('room.save') }}"  method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name_room" class="col-sm-3 control-label">ชื่อห้อง</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="form-group" style="color: red;">
                            <label class="col-sm-3 control-label">ห้องประชุมย่อย</label>
                            <div class="col-sm-5">
                                <select class="form-control" name="children_code">
                                    <option value="0">ไม่มีห้องประชุมย่อย</option>
                                    @foreach($chidrenrooms as $chidrenroom)
                                        <option value="{{ $chidrenroom->id }}">{{ $chidrenroom->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="desc_room" class="col-sm-3 control-label">รายละเอียดห้อง</label>
                            <div class="col-sm-6">
                                <textarea style="resize:none" rows="10" class="form-control" cols="50" id="desc" name="desc">{{ old('desc') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('num') ? ' has-error' : '' }}">
                            <label for="num_room" class="col-sm-3 control-label">จำนวน</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="num" name="num" value="{{ old('num') }}">
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status_room" class="col-sm-3 control-label">สถานะห้อง</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="status" name="status">
                                    <option value="">--- กรุณาเลือก ---</option>
                                    @foreach($status as $status)
                                    <option value="{{ $status->id }}">{{ $status->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('color') ? ' has-error' : '' }}">
                            <label for="color_room" class="col-sm-3 control-label">สีของห้อง</label>
                            <div class="col-sm-1">
                                <input type="color" class="form-control" id="color" name="color" value="{{ old('color') }}">
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('img') ? ' has-error' : '' }}">
                            <label for="img_room" class="col-sm-3 control-label">รูปห้อง</label>
                            <div class="col-sm-4">
                                <input type="file" class="form-control" id="img" name="img" onchange="readURL(this);">
                                <img id="img_room_preview">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-8">
                            <center><button type="submit" class="btn btn-primary" id="btn_create_room" name="btn_create_room">สร้างห้อง</button></center>
                            </div>
                        </div>
                    </form>

            </div>
        </div>

	</div><!-- /.col -->
</div><!-- /.row -->

@include('layouts.inc-minicontent-bottom')
@endsection
@section('scripts')
<script>
function readURL(input)
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#img_room_preview')
                .attr('src', e.target.result)
                .width(400)
                .height(300);
            };

            reader.readAsDataURL(input.files[0]);
    }
}

</script>
@endsection
