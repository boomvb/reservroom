@extends('layouts.layout')

@section('title', 'ระบบจองห้องประชุมสำนักงานอาสากาชาด')

@section('content')

@include('layouts.inc-menu')
@include('layouts.inc-minicontent-top')

<div class="row">
	<div class="col-xs-12">
		<div class="page-header">
			<h1>รายชื่อห้องประชุม</h1>
		</div>
		<!--MessageAlert-->
		@include('flash_msg')
		<!--MessageAlert-->
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<table class="table table-bordered table-striped">
				@foreach($rooms as $room)
					<tr>
						<th colspan="3">{{ $room->id }} : {{ $room->name }}</th>
					</tr>
					<tr>
						<td rowspan="2" width="15%">
							@if($room->images == NULL)
							 	<img src="{{ asset('images/no-image.png') }}" alt="" srcset="">
							@else
								<img src="" alt="" srcset="">
							@endif
						</td>
						<td colspan="3">รายละเอียด : {{ $room->desc }}</td>
					</tr>
					<tr height="35">
						<td>จำนวน : <strong>{{ $room->num }}</strong> ที่นั่ง</td>
						<td>สถานะ : {{ $room->StatusRoom->name }}</td>
					</tr>
					<tr>
						<td colspan="3"><button class="btn btn-white btn-info" type="button" onclick="window.location.href='{{ route('room.edit', $room->id) }}'"><i class="fa fa-pencil fa-fw"></i></button>
						&nbsp;<a class="btn btn-white btn-danger" href="#" onclick="window.location.href='{{ route('room.remove', $room->id) }}'" role="button"><i class="fa fa-trash-o fa-fw"></i></a></td>
					</tr>
				@endforeach
				</table>
			</div>
		</div>
	</div><!-- /.col -->
</div><!-- /.row -->

@include('layouts.inc-minicontent-bottom')
@endsection
