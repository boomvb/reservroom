@extends('layouts.layout')

@section('title', 'ระบบจองห้องประชุมสำนักงานอาสากาชาด')

@section('content')

@include('layouts.inc-menu')
@include('layouts.inc-minicontent-top')

<div class="row">
	<div class="col-xs-12">
        <div class="page-header">
            <h1>รายการการใช้ห้องประชุม</h1>
        </div>
		<!--MessageAlert-->
        @include('flash_msg')
        <!--MessageAlert-->
        @include('reserv.inc-tab')
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade active in" role="tabpanel" id="event_today" aria-labelledby="event_today-tab">
				<p>
                    <div style="margin:auto;width:100%;" class="show_calendar">
                        <div id='calendar'></div>
                    </div>
                    <br>
                    <strong>***หมายเหตุ***</strong><br>
                    @php
                        $rooms = App\Rooms::get();
                        foreach ($rooms as $room) {
                            $arrTextColor = array('สีเขียว','สีส้ม','สีน้ำเงิน','สีแดง','สีดำ','สีน้ำตาล', 'สีม่วง', 'สีชมพู', 'สีฟ้า');
                            $arrColor[] = $room['color'];
                            $arrRoom[] = $room['name'];
                        }
                        $all = count($arrColor);
                            for ($i=0; $i < $all; $i++){
                            print "<strong><font color='$arrColor[$i]'>$arrTextColor[$i] = $arrRoom[$i] </font></strong><br>";
                        }
                    @endphp
                </p>
            </div>
        </div>
	</div><!-- /.col -->
</div><!-- /.row -->
<!-- -->
<div id="fullCalModal" class="modal fade">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
            <h4 id="modalTitle" class="modal-title"></h4> <span id="status" class="label label-success"></span>
        </div>
        <div id="modalBody" class="modal-body">
            <strong>รายละเอียดการจอง</strong>&nbsp;:&nbsp;<p id="desc"></p>
            <strong>วัน/เวลาจอง</strong>&nbsp;:&nbsp;<p><span id="startTime"></span>&nbsp;น.&nbsp;ถึง&nbsp;<span id="endTime"></span>&nbsp;น.</p>
            <strong>ห้องประชุม</strong>&nbsp;:&nbsp;<p id="room"></p><p id="childrenroom"></p>
            <strong>ชื่อผู้จอง</strong>&nbsp;:&nbsp;<p id="name"></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
            <button class="btn btn-primary"><a id="eventUrl" target="_blank" style="color:white;">รายละเอียดเพิ่มเติม</a></button>
        </div>
    </div>
</div>
</div>
<!-- -->
@include('layouts.inc-minicontent-bottom')
@endsection

@section('scripts')
<script>
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listWeek'
        },
        eventRender: function (event, element) {
            element.attr('href', 'javascript:void(0);');
            element.click(function() {
                $("#startTime").html(moment(event.start).format('Do MMMM YYYY HH:mm'));
                $("#endTime").html(moment(event.end).format('Do MMMM YYYY HH:mm'));
                $("#room").html(event.room);
                if (event.childrenroom != null){
                    $("#childrenroom").empty("");
                $.each(event.childrenroom, function( index, value ) {
                    $("#childrenroom").append(value.room.name+'\n');
                });
                }
                $("#name").html(event.name);
                $('#modalTitle').html(event.title);
                $('#desc').html(event.desc);
                $('#eventUrl').attr('href',event.url);
                $("#fullCalModal").modal();
                $("#status").html(event.status);
            });
            if(event.className == 'room1')
            {
                element.css({
                    'background-color': event.color,
                    'border-color' : event.color,
                });
            }
            if(event.className == 'room2')
            {
                element.css({
                    'background-color': event.color,
                    'border-color' : event.color,
                });
            }
            if(event.className == 'room3')
            {
                element.css({
                    'background-color': event.color,
                    'border-color' : event.color,
                });
            }
            if(event.className == 'room4')
            {
                element.css({
                    'background-color': event.color,
                    'border-color' : event.color,
                });
            }
            if(event.className == 'room5')
            {
                element.css({
                    'background-color': event.color,
                    'border-color' : event.color
                });
            }
            if(event.className == 'room6')
            {
                element.css({
                    'background-color': event.color,
                    'border-color' : event.color
                });
            }

            if(event.className == 'room7')
            {
                element.css({
                    'background-color': event.color,
                    'border-color' : event.color
                });
            }

            if(event.className == 'room8')
            {
                element.css({
                    'background-color': event.color,
                    'border-color' : event.color
                });
            }

            if(event.className == 'room9')
            {
                element.css({
                    'background-color': event.color,
                    'border-color' : event.color
                });
            }
        },
        dayClick: function(date, allDay, jsEvent, view) {
            var date = date.format();
            var ex_date = date.split('-');
            var day_reserv = ex_date[2];
            var month_reserv = ex_date[1];
            var year_reserv = ex_date[0];
            window.location.href='{{ route('reserv.create') }}?startday='+day_reserv+'&endday='+day_reserv+'&month='+month_reserv+'&year='+year_reserv
        },
        height: 500,
        buttonIcons:{
            prev: 'left-single-arrow',
            next: 'right-single-arrow',
            prevYear: 'left-double-arrow',
            nextYear: 'right-double-arrow'
        },
        lang: 'th',
        editable: false,
        events : "{{ route('reserv.events') }}",
        eventLimit:true,
    });
</script>
@endsection
