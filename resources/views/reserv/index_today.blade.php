@extends('layouts.layout')

@section('title', 'ระบบจองห้องประชุมสำนักงานอาสากาชาด')

@section('content')

@include('layouts.inc-menu')
@include('layouts.inc-minicontent-top')

<div class="row">
	<div class="col-xs-12">
        <div class="page-header">
            <h1>รายการการใช้ห้องประชุม</h1>
        </div>
		<!--MessageAlert-->
        @include('flash_msg')
        <!--MessageAlert-->
        @include('reserv.inc-tab')
        <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade active in" role="tabpanel" id="event_today" aria-labelledby="event_today-tab">
                    <div class="row">
                        <div class="col-md-12">
                            @foreach ($rooms as $room)
                                <h1>
                                <i class='fa fa-star' aria-hidden='true'></i>&nbsp;{{ $room->name }}
                                </h1>
                                <table class='table table-striped table-bordered' cellspacing='0'>
                                <tr>
                                    <th width='2%'>เลขที่จอง</th>
                                    <th width='5%'>สถานะ</th>
                                    <th width='15%'>หัวข้อประชุม</th>
                                    <th width='10%'>วันที่-เวลาเริ่มต้น</th>
                                    <th width='10%'>วันที่-เวลาสิ้นสุด</th>
                                    <th width='10%'>ชื่อผู้จอง</th>
                                    <th width='10%'>วันที่จอง</th>
                                    <th width='8%' align='center'>&nbsp;</th>
                                </tr>
                                @php
                                    $reservs = App\Reserv::with('status', 'user')
                                        ->where('startday', '=', date('Y-m-d'))
                                        ->where('status_reservs_id', '!=' , 3)
                                        ->where('rooms_id', '=', $room->id)
                                        ->get();
                                @endphp
                                @if(count($reservs) == 0)
                                    <tr>
                                        <td colspan="8"><center><font color="red"><h1>ไม่มีรายการจองในวันนี้!</h1></font></td>
                                    </tr>
                                @endif
                                @foreach ($reservs as $reserv)
                                    <tr>
                                        <td>{{ $reserv->id }}</td>
                                        <td><font color="@if($reserv->status->id == 1) #A0522D @elseif($reserv->status->id == 2) #5cb85c @elseif($reserv->status->id == 3) #d9534f @endif">{{ $reserv->status->name }}</font></td>
                                        <td>{{ $reserv->topic }}</td>
                                        <td>{{ ConvertDate($reserv->startday) }} <br> {{ ConvertTime($reserv->starttime) }}&nbsp;น.</td>
                                        <td>{{ ConvertDate($reserv->endday) }} <br> {{ ConvertTime($reserv->endtime) }}&nbsp;น.</td>
                                        <td>{{ $reserv->user->titles->name }}{{ $reserv->user->fullname }} </td>
                                        <td>@if($reserv->created_at != NULL) {{ ConvertDateCreate($reserv->created_at) }} @endif</td>
                                        <td><center><button class="btn btn-app btn-info btn-xs" onclick="javascript:window.location.href='{{ route('reserv.show',$reserv->id) }}'">
                                        <i class="ace-icon fa fa-info bigger-120"></i>ดูข้อมูล</button></center></center></td>
                                    </tr>
                                @endforeach
                                </table>
                            @endforeach
                        </div>
                    </div>
                </div>
        </div>
	</div><!-- /.col -->
</div><!-- /.row -->

@include('layouts.inc-minicontent-bottom')
@endsection
