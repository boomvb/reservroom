@extends('layouts.layout')

@section('title', 'ระบบจองห้องประชุมสำนักงานอาสากาชาด')

@section('content')

@include('layouts.inc-menu')
@include('layouts.inc-minicontent-top')
@php
//---ConvertDate---//
    $date_year = date("Y");
    $date_month = date("n");
    $date_day = date("d");
    $date_year = $date_year+543;
@endphp
<div class="row">
	<div class="col-xs-12">
        <div class="page-header">
            <h1>จองห้องประชุม</h1>
        </div>
        <!--MessageAlert-->
        @include('flash_msg')
        <!--MessageAlert-->
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2 class="panel-title">แบบฟอร์มจองห้องประชุมสำนักงานอาสากาชาด</h2>
            </div>
            <div class="panel-body">
                <form action="{{ route('reserv.update') }}" method="post" class="form-horizontal">
                {{ csrf_field() }}
                <div class="page-header">
                    <h3>วัน/เวลาที่ใช้</h3>
                </div>
                <div class="form-group">
                    <label for="startday" class="col-sm-1 control-label">วันที่เริ่มต้น :</label>
                    <div class="col-sm-5">
                        <select id="startday" name="startday">
                            <option value="">--- เลือกวัน ---</option>
                            @php for($day=01;$day<=31;$day++)
                            {
                                if($date_day == $day)
                                {
                                    $cur_strday = "selected";
                                }else
                                {
                                    $cur_strday = "";
                                }
                            @endphp
                                <option value="{{ $day }}" @if(ConvertDay($reservs->startday) == $day) selected @endif>{{ $day }}</option>
                            @php } @endphp
                        </select>
                        <select id="startmonth" name="startmonth">
                            <option value="">--- เลือกเดือน ---</option>
                            @php
                            $month = array(
                                '1' => "มกราคม",
                                '2' => "กุมภาพันธ์",
                                '3' => "มีนาคม",
                                '4' => "เมษายน",
                                '5' => "พฤษภาคม",
                                '6' => "มิถุนายน",
                                '7' => "กรกฎาคม",
                                '8' => "สิงหาคม",
                                '9' => "กันยายน",
                                '10' => "ตุลาคม",
                                '11' => "พฤศจิกายน",
                                '12' => "ธันวาคม",
                            );
                            foreach($month as $key => $value)
                            {
                                $ex_date = explode("-",$reservs->startday);
                                $newStrMonth = $ex_date[1];

                            @endphp
                                <option value="{{ $key }}" @if($newStrMonth == $key) selected @endif>{{ $value }}</option>
                            @php
                            }
                            @endphp
                        </select>
                        <select id="startyear" name="startyear">
                            <option value="">--- เลือกปี ---</option>
                            @php
                            for($i=2550;$i<=$date_year+10;$i++){
                            @endphp
                                <option value="{{ $i }}" @if(ConvertYear($reservs->startday) == $i) selected @endif> {{ $i }} </option>
                            @php } @endphp
                        </select>
                    </div>
                    <label for="endday" class="col-sm-1 control-label">วันที่สิ้นสุด :</label>
                    <div class="col-sm-5">
                    <select id="endday" name="endday">
                            <option value="">--- เลือกวัน ---</option>
                            @php
                                for($day=01;$day<=31;$day++){
                            @endphp
                                <option value="{{ $day }}" @if(ConvertDay($reservs->endday) == $day) selected @endif >{{ $day }}</option>
                            @php } @endphp
                        </select>
                        <select id="endmonth" name="endmonth">
                            <option value="">--- เลือกเดือน ---</option>
                            @php
                            $month = array(
                                '1' => "มกราคม",
                                '2' => "กุมภาพันธ์",
                                '3' => "มีนาคม",
                                '4' => "เมษายน",
                                '5' => "พฤษภาคม",
                                '6' => "มิถุนายน",
                                '7' => "กรกฎาคม",
                                '8' => "สิงหาคม",
                                '9' => "กันยายน",
                                '10' => "ตุลาคม",
                                '11' => "พฤศจิกายน",
                                '12' => "ธันวาคม",
                            );
                            foreach($month as $key => $value)
                            {
                                $ex_date = explode("-",$reservs->endday);
                                $newStrMonth = $ex_date[1];

                            @endphp
                                <option value="{{ $key }}" @if($newStrMonth == $key) selected @endif>{{ $value }}</option>
                            @php
                            }
                            @endphp
                        </select>
                        <select id="endyear" name="endyear">
                            <option value="">--- เลือกปี ---</option>
                            @php
                                for($i=2550;$i<=$date_year+10;$i++){
                            @endphp
                            <option value="{{ $i }}" @if(ConvertYear($reservs->endday) == $i) selected @endif>{{ $i }}</option>
                            @php
                            }
                            @endphp
                        </select>
                    </div>
                </div>
                @php
                function hoursRange( $lower = 0, $upper = 86400, $step = 3600, $format = '24' ) {
                    $times = array();

                    if ( empty( $format ) ) {
                        $format = 'G:i';
                    }

                    foreach ( range( $lower, $upper, $step ) as $increment ) {
                        $increment = gmdate( 'H:i', $increment );

                        list( $hour, $minutes ) = explode( ':', $increment );

                        $date = new DateTime( $hour . ':' . $minutes );

                        $times[(string) $increment] = $date->format( $format );
                    }

                    return $times;
                }
                    $times = hoursRange(21600, 63000, 60 * 15, 'H:i');
                @endphp
                <div class="form-group">
                    <label for="starttime" class="col-sm-1 control-label">เวลา :</label>
                    <div class="col-sm-5">
                        <select id="starttime" name="starttime" class="{{ $errors->has('starttime') ? ' has-error' : '' }}">
                            <option value="">---</option>
                            @php
                            foreach($times as $starttime)
                            {
                            @endphp
                                <option value="{{ $starttime }}" @if(ConvertTime($reservs->starttime) == $starttime) selected @endif>{{ $starttime }} </option>
                            @php
                            }
                            @endphp
                        </select>
                        <label for="">น.</label>
                    </div>
                    <label for="endtime" class="col-sm-1 control-label">สิ้นสุดเวลา :</label>
                    <div class="col-sm-5">
                        <select id="endtime" name="endtime" class="{{ $errors->has('endtime') ? ' has-error' : '' }}">
                            <option value="">---</option>
                            @php
                            foreach($times as $endtime)
                            {
                            @endphp
                                <option value="{{ $endtime }}" @if(ConvertTime($reservs->endtime) == $endtime) selected @endif>{{ $endtime }}</option>
                            @php
                            }
                            @endphp
                        </select>
                        <label for="">น.</label>
                    </div>
                </div>
                <div class="page-header">
                    <h3>รายละเอียดห้องประชุม</h3>
                    <div class="form-group{{ $errors->has('room') ? ' has-error' : '' }}">
                        <label for="room" class="col-sm-2 control-label">ห้องประชุม :</label>
                        <div class="col-sm-3">
                        <select class="form-control" id="room" name="room">
                            <option value="">--- กรุณาเลือกห้อง ---</option>
                            @foreach($rooms as $room)
                            <option value="{{ $room->id }}" @if($reservs->rooms_id == $room->id) selected @else '' @endif>{{ $room->name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="page-header">
                    <h3>รายละเอียดการจอง</h3>
                    <div class="form-group{{ $errors->has('typereserv') ? ' has-error' : '' }}">
                        <label for="typereserv" class="col-sm-2 control-label">ประเภทงานประชุม :</label>
                        <div class="col-sm-2">
                            <select class="form-control" id="typereserv" name="typereserv">
                                <option value="">--- กรุณาเลือก ---</option>
                                @foreach($types as $type)
                                <option value="{{ $type->id }}" @if($reservs->types_id == $type->id) selected @endif>{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('topic') ? ' has-error' : '' }}">
                        <label for="topic" class="col-sm-2 control-label">หัวข้อ :</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="topic" name="topic" value="{{ $reservs->topic }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="desc" class="col-sm-2 control-label">รายละเอียด :</label>
                        <div class="col-sm-5">
                            <textarea class="form-control" style="resize:none;" rows="10" cols="30" id="desc" name="desc">{{ $reservs->desc }}</textarea>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('num') ? ' has-error' : '' }}">
                        <label for="num" class="col-sm-2 control-label">จำนวนผู้เข้าประชุม :</label>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" id="num" name="num" onkeyPress="CheckNum()" value="{{ $reservs->num }}">
                        </div>
                        <label for="" class="control-label">คน</label>
                    </div>
                    <div class="form-group">
                        <label for="namejoin" class="col-sm-2 control-label">รายชื่อผู้เข้าร่วมประชุม :</label>
                        <div class="col-sm-2">
                            <textarea class="form-control" rows="8" cols="40" style="resize:none;" id="namejoin" name="namejoin">{{ $reservs->namejoin }}</textarea>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('num') ? ' has-error' : '' }}">
                        <label for="tel" class="col-sm-2 control-label">เบอร์โทรศัพท์ :</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="tel" name="tel" value="{{ $reservs->tel }}">
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('check_catering') ? ' has-error' : '' }}">
                        <label for="catering" class="col-sm-2 control-label">จัดเลี้ยงอาหาร :</label>
                        <div class="col-sm-8">
                            <div class="radio">
                            <label>
                                <input type="radio" name="check_catering" id="check_catering1" value="1" @if($reservs->check_catering == 1) checked @endif>
                                จัดเลี้ยงเอง/มีอุปกรณ์มาเอง
                            </label>
                            </div>
                            <div class="radio">
                            <label>
                                <input type="radio" name="check_catering" id="check_catering2" value="2" @if($reservs->check_catering == 2) checked @endif>
                                จัดเลี้ยงเอง ยืมอุปกรณ์
                                <input type="text" class="form-control" id="txt_catering2" name="txt_catering2" disabled="disabled" value="{{ $reservs->txt_catering2 }}">
                            </label>
                            </div>
                            <div class="radio">
                            <label>
                                <input type="radio" name="check_catering" id="check_cateringother" value="3" @if($reservs->check_catering == 3) checked @endif>
                                อื่นๆ
                                <input type="text" class="form-control" id="txt_cateringother" name="txt_cateringother" disabled="disabled" value="{{ $reservs->txt_cateringother }}">
                            </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">อุปกรณ์โสตฯที่ต้องการใช้ :</label>
                        <div class="col-sm-7">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_projector" id="check_projector" value="1" @if($reservs->check_projector == 1) checked @endif>
                                เครื่องโปรเจคเตอร์
                            </label>
                            <label>
                                <input type="checkbox" name="check_screen" id="check_screen" value="1" @if($reservs->check_screen == 1) checked @endif>
                                จอภาพ
                            </label>
                            <label>
                                <input type="checkbox" name="check_dvd" id="check_dvd" value="1" @if($reservs->check_dvd == 1) checked @endif>
                                เครื่องเล่น DVD
                            </label>
                            <label>
                                <input type="checkbox" name="check_tv" id="check_tv" value="1" @if($reservs->check_tv == 1) checked @endif>
                                โทรทัศน์
                            </label>
                            <label>
                                <input type="checkbox" name="check_record" id="check_record" value="1" @if($reservs->check_record == 1) checked @endif>
                                เครื่องบันทึกเสียง
                            </label>
                            <label>
                                <input type="checkbox" name="check_amp" id="check_amp" value="1" @if($reservs->check_amp == 1) checked @endif>
                                เครื่องขยายเสียง
                            </label>
                            </div>
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_control" id="check_control" value="1" @if($reservs->check_control == 1) checked @endif>
                                เจ้าหน้าที่ควบคุม (คน)
                                <input type="text" class="form-control" id="txt_control" name="txt_control" disabled="disabled" onkeyPress="CheckNum()">
                            </label>
                            </div>
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_wireless_mic" id="check_wireless_mic" value="1">
                                ไมโครโฟนไร้สาย (ชุด)
                                <input type="text" class="form-control" id="txt_wireless_mic" name="txt_wireless_mic" disabled="disabled" onkeyPress="CheckNum()">
                            </label>
                            </div>
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_mic" id="check_mic" value="1">
                                ไมโครโฟนยืน (ชุด)
                                <input type="text" class="form-control" id="txt_mic" name="txt_mic" disabled="disabled" onkeyPress="CheckNum()" value="{{ $reservs->txt_mic }}">
                            </label>
                            </div>
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_other" id="check_other" value="1">
                                อื่นๆ (ระบุ)
                                <input type="text" class="form-control" id="txt_other" name="txt_other" disabled="disabled" value="{{ $reservs->txt_other }}">
                            </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('position_reserv') ? ' has-error' : '' }}">
                        <label for="" class="col-sm-2 control-label">รูปแบบการจัดห้องประชุม :</label>
                        <div class="col-sm-7">
                            @foreach($positions as $position)
                                <div class='radio-inline'>
                                    <label>
                                    <input type='radio' name='position_reserv' id='position_reserv_{{ $position->id }}' value="{{ $position->id }}" @if($reservs->position_reservs_id == $position->id) checked @endif>
                                    {{ $position->name }}
                                    </label>
                                </div>
                            @endforeach
                            &nbsp;&nbsp;<input type='text' name='txt_tableReserv' id='txt_tableReserv' disabled='disabled'>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-1 col-sm-8">
                        <input type="hidden" name="id_reserv" value="{{ $reservs->id }}">
                        <input type="hidden" name="id_status" value="{{ $reservs->status_reservs_id }}">
                    <center><button type="submit" class="btn btn-success" id="btn_edit_reserv" name="btn_edit_reserv" value="add">แก้ไขข้อมูลการจอง</button></center>
                    </div>
                </div>
                </form>
                </div>
            </div>

	</div><!-- /.col -->
</div><!-- /.row -->

@include('layouts.inc-minicontent-bottom')
@endsection
@section('scripts')
<script>
    $("#check_control").click(function(){
        if(this.checked)
        {
            $("#txt_control").removeAttr('disabled','disabled')
            $("#txt_control").focus();
        }else
        {
            $("#txt_control").attr('disabled','disabled')
            $("#txt_control").val("")
        }
    });
    $("#check_wireless_mic").click(function(){
        if(this.checked)
        {
            $("#txt_wireless_mic").removeAttr('disabled','disabled')
            $("#txt_wireless_mic").focus();
        }else
        {
            $("#txt_wireless_mic").attr('disabled','disabled')
            $("#txt_wireless_mic").val("")
        }
    });
    $("#check_mic").click(function(){
        if(this.checked)
        {
            $("#txt_mic").removeAttr('disabled','disabled')
            $("#txt_mic").focus();
        }else
        {
            $("#txt_mic").attr('disabled','disabled')
            $("#txt_mic").val("")
        }
    });
    $("#check_other").click(function(){
        if(this.checked)
        {
            $("#txt_other").removeAttr('disabled','disabled');
            $("#txt_other").focus();
        }else
        {
            $("#txt_other").attr('disabled','disabled');
            $("#txt_other").val("");
        }
    });
    $("#check_catering2").click(function(){
        if(this.checked)
        {
            $("#txt_cateringother").attr('disabled','disabled')
            $("#txt_cateringother").val("")
            $("#txt_catering2").removeAttr('disabled','disabled')
            $("#txt_catering2").focus()
        }else
        {
            $("#txt_catering2").attr('disabled','disabled')
            $("#txt_catering2").val("")
        }
    });
    $("#check_cateringother").click(function(){
        if(this.checked)
        {
            $("#txt_catering2").attr('disabled','disabled')
            $("#txt_catering2").val("")
            $("#txt_cateringother").removeAttr('disabled','disabled')
            $("#txt_cateringother").focus()
        }else
        {
            $("#txt_cateringother").attr('disabled','disabled')
            $("#txt_cateringother").val("")
        }
    });
function CheckNum(){
	if (event.keyCode < 48 || event.keyCode > 57){
		    event.returnValue = false;
	    }
    }
function autoTel(obj,typeCheck){
        if(typeCheck==1){
            var pattern=new String("_-____-_____-_-__");
            var pattern_ex=new String("-");
        }else{
            var pattern=new String("__-____-____");
            var pattern_ex=new String("-");
        }
        var returnText=new String("");
        var obj_l=obj.value.length;
        var obj_l2=obj_l-1;
        for(i=0;i<pattern.length;i++){
            if(obj_l2==i && pattern.charAt(i+1)==pattern_ex){
                returnText+=obj.value+pattern_ex;
                obj.value=returnText;
            }
        }
        if(obj_l>=pattern.length){
            obj.value=obj.value.substr(0,pattern.length);
        }
}

$("input[name='position_reserv']").click(function(){
    var IdVal = $("input[name='position_reserv']:checked").val();
    if(IdVal == 3)
    {
        $("#txt_tableReserv").removeAttr('disabled','disabled')
        $("#txt_tableReserv").focus()
    }else
    {
        $("#txt_tableReserv").attr('disabled','disabled')
        $("#txt_tableReserv").val("")
    }
});
</script>
@endsection
