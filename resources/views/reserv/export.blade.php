<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>เลขที่จอง : {{ $reservs->id }} {{ $reservs->topic }}</title>
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ asset('public/fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ asset('public/fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ asset('public/fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ asset('public/fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }
        body {
            font-family: "THSarabunNew";
        }
        .page-break {
            page-break-after: always;
        }
        span.text-dt {
            border-bottom: 1px dotted #000;
            text-decoration: none;
            width:100px;height:100px;
        }
        .line{
            margin: 10 0 10 0px;
            border-bottom: 1px dotted #000;
        }
    </style>
</head>
<body>
    <center>
        <img style="margin-bottom:-30px;margin-top:-20px" src="{{ $logo }}" alt="{{ $logo }}">
        <h2 style="text-align:center;font-size:20pt;margin-bottom:10px;">แบบคำขอใช้ห้องประชุม สำนักงานอาสากาชาด</h2>
    </center>
    <table width="100%" style="border:1px solid #000000;font-size:15pt;" cellPadding="1">
                <tr>
                    <td colspan="2" align="right">
                        เลขที่จอง : <strong><span class="text-dt">{{ $reservs->id }}</span></strong><br>
                        สถานะ : <strong>
                            <font color="@if($reservs->status->id == 1) #A0522D @elseif($reservs->status->id == 2) #5cb85c @elseif($reservs->status->id == 3) #d9534f @endif"><span class="text-dt">{{ $reservs->status->name }}</span></font></strong>
                            <br>
                            @if($reservs->status->id == 3)
                                หมายเหตุ : <span class="text-dt">{{ $reservs->note }}</span>
                            @endif
                        </p>
                    </td>
                </tr>
                <tr style="border:1px solid #000000;">
                    <td colspan="2">
                        เรื่อง : <strong><span class="text-dt">{{ $reservs->topic }}</span></strong>
                    </td>
                </tr>
                <tr style="border:1px solid #000000;">
                    <td colspan="2">
                        รายละเอียด : <span class="text-dt">{{ $reservs->desc }}</span>
                    </td>
                </tr>
                <tr style="border:1px solid #000000;">
                    <td>
                        ห้องประชุม : <span class="text-dt">{{ $reservs->room->name }}
                                    @if (count($reservs->RoomsReserv) != 0)
                                        (
                                        @foreach($reservs->RoomsReserv as $roomreserv)
                                            {{ $roomreserv->room->name }}
                                        @endforeach
                                        )
                                    @endif
                        </span>
                    </td>
                    <td>
                        จำนวน : <span class="text-dt">{{ $reservs->num }}</span>&nbsp;คน</ins>
                    </td>
                </tr>
                <tr style="border:1px solid #000000;">
                    <td colspan="2">
                        รายชื่อผู้เข้าประชุม : <span class="text-dt">{{ $reservs->namejoin }}</span>
                    </td>
                </tr>
                <tr style="border:1px solid #000000;">
                    <td colspan="2">
                        วันที่เริ่มต้น: <strong><span class="text-dt">{{ ConvertDay($reservs->startday) }}</span></strong>&nbsp;เดือน&nbsp;<strong><span class="text-dt">{{ ConvertMonth($reservs->startday) }}</span></strong>&nbsp;พ.ศ.&nbsp;<strong><span class="text-dt">{{ ConvertYear($reservs->startday) }}</span></strong>
                        วันที่สิ้นสุด: <strong><span class="text-dt">{{ ConvertDay($reservs->endday) }}</span></strong>&nbsp;เดือน&nbsp;<strong><span class="text-dt">{{ ConvertMonth($reservs->endday) }}</span></strong>&nbsp;พ.ศ.&nbsp;<strong><span class="text-dt">{{ ConvertYear($reservs->endday) }}</span></strong>
                    </td>
                </tr>
                <tr style="border:1px solid #000000;">
                    <td colspan="2">
                        เวลา <strong><span class="text-dt">{{ ConvertTime($reservs->starttime) }}</span></strong>&nbsp;น. สิ้นสุดเวลา <strong><span class="text-dt">{{ ConvertTime($reservs->endtime) }}</span></strong> น.
                    </td>
                </tr>
                <tr style="border:1px solid #000000;">
                    <td colspan="2">
                        จัดเลี้ยงอาหาร : <strong><span class="text-dt">
                            @if($reservs->check_catering == 1)
                                จัดเลี้ยงเอง/มีอุปกรณ์มาเอง
                            @elseif($reservs->check_catering == 2)
                                จัดเลี้ยงเอง ยืมอุปกรณ์
                            @elseif($reservs->check_catering == 3)
ื่                                  อื่นๆ
                            @else

                            @endif
                            </span></strong>
                            @if($reservs->check_catering == 2)
                                ระบุ ( {{ $reservs->txt_catering2 }} )
                            @elseif($reservs->check_catering == 3)
                                ระบุ ( {{ $reservs->txt_cateringother }} )
                            @endif

                    </td>
                </tr>
                <tr style="border:1px solid #000000;">
                    <td colspan="2">
                        อุปกรณ์โสตฯที่ต้องการใช้ :
                            @php
                                $arr_data = array(
                                    'เครื่องโปรเจคเตอร์' => $reservs->check_projector,
                                    'จอภาพ' => $reservs->check_screen,
                                    'เครื่องเล่น DVD' => $reservs->check_dvd,
                                    'โทรทัศน์' => $reservs->check_tv,
                                    'เครื่องบันทึกเสียง' => $reservs->check_record,
                                    'เครื่องขยายเสียง' => $reservs->check_amp,
                                );
                                foreach($arr_data as $key_data => $val_data)
                                {
                                    if($val_data == 1)
                                    {
                                        echo "<br><strong>-&nbsp;";
                                        echo "<span class='text-dt'>";
                                        print_r($key_data);
                                        echo "</span></strong>";
                                    }
                                }
                                $arr_data1 =array(
                                    'เจ้าหน้าที่ควบคุม' => array(
                                        $reservs->txt_control.'&nbsp;คน' => $reservs->check_control,
                                    ),
                                    'ไมโครโฟนไร้สาย' => array(
                                        $reservs->txt_wireless_mic.'&nbsp;ชุด' => $reservs->check_wireless_mic
                                    ),
                                    'ไมโครโฟนยืน' => array(
                                        $reservs->txt_mic.'&nbsp;ชุด' => $reservs->check_mic,
                                    ),
                                    'อื่นๆ' => array(
                                        $reservs->txt_other => $reservs->check_other,
                                    ),

                                );
                                foreach($arr_data1 as $key_data1 => $val_data1)
                                {
                                    foreach($val_data1 as $txt_data => $txt_key)
                                    {
                                        if($txt_key == 1)
                                        {
                                            echo "<br><strong>-&nbsp;";
                                            echo "<span class='text-dt'>";
                                            print_r($key_data1);
                                            echo "&nbsp;";
                                            print_r($txt_data);
                                            echo "</span></strong>";
                                        }
                                    }
                                }
                            @endphp
                    </td>
                </tr>
                <tr style="border:1px solid #000000;">
                    <td colspan="2">
                        รูปแบบการจัดห้องประชุม : <strong><span class="text-dt">{{ $reservs->position->name }}&nbsp;&nbsp;@php if($reservs->position->id == 3){ echo "<strong>(".$reservs->note_position_reserv.")</strong>"; } @endphp</span></strong>
                    </td>
                </tr>
                <tr style="border:1px solid #000000;">
                    <td>
                        ชื่อผู้จอง : <strong><span class="text-dt">{{ $reservs->user->titles->name }} {{ $reservs->user->fullname }}</span></strong>
                    </td>
                    <td>
                        วันที่จอง : <strong><span class="text-dt">@if($reservs->created_at != NULL) {{ ConvertDateCreate($reservs->created_at) }} @endif</span></strong>
                    </td>
                </tr>
                <tr style="border:1px solid #000000;">
                    <td>
                        หน่วยงาน/ฝ่าย : <strong><span class="text-dt">{{ $reservs->user->group->name }}</span></strong>
                    </td>
                    <td>
                        เบอร์โทรติดต่อ : <strong><span class="text-dt">{{ $reservs->tel }}</span></strong>
                    </td>
                </tr>
            </table>
            <strong>***หมายเหตุ :
            <br>1. สำนักงานอาสากาชาดไม่สะดวกในการให้ยืมเครื่อง Projector และคอมพิวเตอร์ ผู้ขอให้ห้องประชุมโปรดนำมาเอง<br>
            2. ผู้ขอให้ห้องประชุมต้องจัดเจ้าหน้าที่/พนักงานไปจัดสถานที่ล่วงหน้าและจัดเก็บให้ตามสมควร โดยให้สำนักงานอาสากาชาดจะประสานงานและอำนวยการความสะดวกให้<br>
            3. ในกรณีที่ขอให้ห้องประชุมนอกเวลาราชการ หรือในวันหยุดราชการ ผู้ขอให้ห้องประชุมต้องรับผิดชอบค่าปฏิบัติงานนอกเวลาราชการหรือวันหยุดราชการด้วย<br>
            </strong>
            <span style="text-align:right;font-size:10pt;margin-top:22px;">ระบบจองห้องประชุม สำนักงานอาสากาชาดสภากาชาดไทย</span>
</body>
</html>
