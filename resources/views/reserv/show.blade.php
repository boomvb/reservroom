@extends('layouts.layout')

@section('title', 'ระบบจองห้องประชุมสำนักงานอาสากาชาด')

@section('content')

@include('layouts.inc-menu')
@include('layouts.inc-minicontent-top')

@section('stylesheets')
<style>
#content_reserv table tr td p
{
    font-size: 1.0em;
    margin-top: 11px;
}
</style>
@endsection
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-body">
        <div id="content_reserv">
            <center>
            <img src="{{ $logo }}" alt="{{ $logo }}">
                <h2>แบบคำขอใช้ห้องประชุม สำนักงานอาสากาชาด</h2>
            </center>
            <table class="table table-bordered">
                <tr>
                    <td colspan="2">
                        <p class="text-right">เลขที่จอง : <strong><ins>{{ $reservs->id }}</ins></strong></p>
                        <p class="text-right">สถานะ : <strong>
                            <ins>
                            <font color="@if($reservs->status->id == 1) #A0522D @elseif($reservs->status->id == 2) #5cb85c @elseif($reservs->status->id == 3) #d9534f @endif">{{ $reservs->status->name }}</font>
                            <br>
                            @if($reservs->status->id == 3)
                                หมายเหตุ : {{ $reservs->note }}
                            @endif
                            </ins></strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <p class="text-left">เรื่อง : <strong><ins>{{ $reservs->topic }}</ins></strong></p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <p class="text-left">รายละเอียด : <strong><ins>{{ $reservs->desc }}</ins></strong></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="text-left">ห้องประชุม : <strong><ins>{{ $reservs->room->name }}
                                    @if (count($reservs->RoomsReserv) != 0)
                                        (
                                        @foreach($reservs->RoomsReserv as $roomreserv)
                                            {{ $roomreserv->room->name }}
                                        @endforeach
                                        )
                                        @endif
                                </ins></strong></p>
                    </td>
                    <td>
                        <p class="text-left">จำนวน : <strong><ins>{{ $reservs->num }}</ins></strong>&nbsp;คน</ins></p>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        <p class="text-left">รายชื่อผู้เข้าประชุม : <strong><ins>{{ $reservs->namejoin }}</ins></strong></p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <p class="text-left">วันที่เริ่มต้น : <strong><ins>{{ ConvertDay($reservs->startday) }}</ins></strong>&nbsp;เดือน&nbsp;<strong><ins>{{ ConvertMonth($reservs->startday) }}</ins></strong>&nbsp;พ.ศ.<strong><ins>{{ ConvertYear($reservs->startday) }}</ins></strong>
                        วันที่สิ้นสุด : <strong><ins>{{ ConvertDay($reservs->endday) }}</ins></strong>&nbsp;เดือน&nbsp;<strong><ins>{{ ConvertMonth($reservs->endday) }}</ins></strong>&nbsp;พ.ศ.<strong><ins>{{ ConvertYear($reservs->endday) }}</ins></strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <p class="text-left">เวลา : <strong><ins>{{ ConvertTime($reservs->starttime) }}</ins></strong>&nbsp;น. สิ้นสุดเวลา : <strong><ins>{{ ConvertTime($reservs->endtime) }}</ins></strong>&nbsp;น.</p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <p class="text-left">จัดเลี้ยงอาหาร : <ins>
                            @if($reservs->check_catering == 1)
                                จัดเลี้ยงเอง/มีอุปกรณ์มาเอง
                            @elseif($reservs->check_catering == 2)
                                จัดเลี้ยงเอง ยืมอุปกรณ์
                            @elseif($reservs->check_catering == 3)
ื่                                  อื่นๆ
                            @else

                            @endif
                            </ins>
                            @if($reservs->check_catering == 2)
                                ระบุ ( {{ $reservs->txt_catering2 }} )
                            @elseif($reservs->check_catering == 3)
                                ระบุ ( {{ $reservs->txt_cateringother }} )
                            @endif
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <p class="text-left">อุปกรณ์โสตฯที่ต้องการใช้ :
                            @php
                                $arr_data = array(
                                    'เครื่องโปรเจคเตอร์' => $reservs->check_projector,
                                    'จอภาพ' => $reservs->check_screen,
                                    'เครื่องเล่น DVD' => $reservs->check_dvd,
                                    'โทรทัศน์' => $reservs->check_tv,
                                    'เครื่องบันทึกเสียง' => $reservs->check_record,
                                    'เครื่องขยายเสียง' => $reservs->check_amp,
                                );
                                foreach($arr_data as $key_data => $val_data)
                                {
                                    if($val_data == 1)
                                    {
                                        echo "<br>-&nbsp;";
                                        echo "<ins>";
                                        print_r($key_data);
                                        echo "</ins>";
                                    }
                                }
                                $arr_data1 =array(
                                    'เจ้าหน้าที่ควบคุม' => array(
                                        $reservs->txt_control.'&nbsp;คน' => $reservs->check_control,
                                    ),
                                    'ไมโครโฟนไร้สาย' => array(
                                        $reservs->txt_wireless_mic.'&nbsp;ชุด' => $reservs->check_wireless_mic
                                    ),
                                    'ไมโครโฟนยืน' => array(
                                        $reservs->txt_mic.'&nbsp;ชุด' => $reservs->check_mic,
                                    ),
                                    'อื่นๆ' => array(
                                        $reservs->txt_other => $reservs->check_other,
                                    ),

                                );
                                foreach($arr_data1 as $key_data1 => $val_data1)
                                {
                                    foreach($val_data1 as $txt_data => $txt_key)
                                    {
                                        if($txt_key == 1)
                                        {
                                            echo "<br>-&nbsp;";
                                            echo "<ins>";
                                            print_r($key_data1);
                                            echo "&nbsp;";
                                            print_r($txt_data);
                                            echo "</ins>";
                                        }
                                    }
                                }
                            @endphp
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <p class="text-left">รูปแบบการจัดห้องประชุม : <ins>{{ $reservs->position->name }}</ins></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="text-left">ชื่อผู้จอง : <ins>{{ $reservs->user->titles->name }} {{ $reservs->user->fullname }}</ins></p>
                    </td>
                    <td>
                        <p class="text-left">วันที่จอง : <ins>@if($reservs->created_at != NULL) {{ ConvertDateCreate($reservs->created_at) }} @endif</ins></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="text-left">หน่วยงาน/ฝ่าย : <ins>{{ $reservs->user->group->name }}</ins></p>
                    </td>
                    <td>
                        <p class="text-left">เบอร์โทรติดต่อ : <strong><ins>{{ $reservs->tel }}</ins></strong></p>
                    </td>
                </tr>
            </table>
            <p style="font-size:1.0em">
            <strong>***หมายเหตุ :
            <br>1. สำนักงานอาสากาชาดไม่สะดวกในการให้ยืมเครื่อง Projector และคอมพิวเตอร์ ผู้ขอใช้ห้องประชุมโปรดนำมาเอง<br>
            2. ผู้ขอใช้ห้องประชุมต้องจัดเจ้าหน้าที่/พนักงานไปจัดสถานที่ล่วงหน้าและจัดเก็บให้ตามสมควร โดยให้สำนักงานอาสากาชาดจะประสานงานและอำนวยการความสะดวกให้<br>
            3. ในกรณีที่ขอใช้ห้องประชุมนอกเวลาราชการ หรือในวันหยุดราชการ ผู้ขอใช้ห้องประชุมต้องรับผิดชอบค่าปฏิบัติงานนอกเวลาราชการหรือวันหยุดราชการด้วย<br>
            </strong>
            </p>
            <p>&nbsp;</p>
        @php
            $func_onclick = "";
            $disabled_btn = "";
            $attr_btncolor = "";
            $text_btn = "";
            $icon_btn = "";
        @endphp

        @if(Auth::user()->roles_id == 1 || Auth::user()->roles_id == 2)

            @if($reservs->status_reservs_id == 1)

            @php
                $func_onclick = "confirm_reserv";
                $disabled_btn = "";
                $attr_btncolor = "success";
                $text_btn = "อนุมัติ";
                $icon_btn = "check";
            @endphp

            @elseif($reservs->status_reservs_id == 2)

            @php
                $func_onclick = "cancle_reserv";
                if(Auth::user()->id != $reservs->user->id){
                    $disabled_btn = "";
                }else{
                    $disabled_btn = "";
                }
                $attr_btncolor = "danger";
                $text_btn = "ยกเลิก";
                $icon_btn = "remove";
            @endphp

            @elseif($reservs->status_reservs_id == 3)

            @php
                $func_onclick = "cancle_reserv";
                $disabled_btn = "disabled";
                $attr_btncolor = "danger";
                $text_btn = "ยกเลิก";
                $icon_btn = "remove";
            @endphp

            @endif

        @endif
        @if(Auth::user()->roles_id == 3)

            @if($reservs->status_reservs_id == 1)

                @php
                    $func_onclick = "cancle_reserv";
                    if(Auth::user()->id != $reservs->user->id){
                        $disabled_btn = "disabled";
                    }else{
                        $disabled_btn = "";
                    }
                    $attr_btncolor = "danger";
                    $text_btn = "ยกเลิก";
                    $icon_btn = "remove";
                @endphp

                @elseif($reservs->status_reservs_id == 2)

                @php
                    $func_onclick = "cancle_reserv";
                    if(Auth::user()->id != $reservs->user->id){
                        $disabled_btn = "disabled";
                    }else{
                        $disabled_btn = "";
                    }
                    $attr_btncolor = "danger";
                    $text_btn = "ยกเลิก";
                    $icon_btn = "remove";
                @endphp

                @elseif($reservs->status_reservs_id == 3)

                @php
                    $func_onclick = "cancle_reserv";
                    $disabled_btn = "disabled";
                    $attr_btncolor = "danger";
                    $text_btn = "ยกเลิก";
                    $icon_btn = "remove";
                @endphp

                @endif

        @endif
        <center>
        <button class="btn btn-app btn-yellow btn-xs" @if(Auth::user()->id != $reservs->user->id) disabled @endif onclick="window.location.href='{{ route('reserv.edit',$reservs->id) }}'"><i class="ace-icon fa fa-pencil bigger-160"></i>แก้ไข</button>
        <button class="btn btn-app btn-{{ $attr_btncolor }} btn-xs" {{ $disabled_btn }} onclick="{{ $func_onclick }}('{{ $reservs->id }}')" ><i class="ace-icon fa fa-{{ $icon_btn }} bigger-200"></i>{{ $text_btn }}</button>
        <button class="btn btn-app btn-pink btn-xs" onclick="window.location.href='{{ route('reserv.export',$reservs->id) }}'"><i class="ace-icon fa fa-file-pdf-o bigger-160"></i>Export</button>
        </center>


        </div>
        </div>
    </div>
</div>

@include('layouts.inc-minicontent-bottom')


@endsection
@section('scripts')
    <script>
    $( function() {
        $("#click-dialog").click(function(){
            $( "#dialog-confirm" ).dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Delete all items": function() {
                $( this ).dialog( "close" );
                },
                Cancel: function() {
                $( this ).dialog( "close" );
                }
            }
            });
        })
    } );
    </script>

    <script>
        function confirm_reserv(id){
            swal({
                title: "ยืนยันการอนุมัติการจองประชุม?",
                text: "เลขที่จอง "+id,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('reserv.updateStatus') }}",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id : id
                        },
                        dataType: "JSON",
                        success: function(data){
                            if(data.success == true){
                                swal("ยืนยันการจองห้องเรียบร้อยแล้ว!","","success")
                                .then(function(){
                                    location.reload();
                                });
                            }else{
                                swal("Error!", "กรุณาลองใหม่อีกครั้ง!", "error");
                            }
                        }
                    });
                } else {

                }
            });
        }
        function cancle_reserv(id,status){
           bootbox.prompt("หมายเหตุยกเลิกการจอง ?", function(result) {
                if(result !== null){
                    var comment_reserv = $(".comment_reserv").val();
                    if(comment_reserv == ""){
                        swal("กรุณาระบุหมายเหตุยกเลิกการจองด้วยครับ!","","error")
                        $(".comment_reserv").focus();
                        return false;
                    }else{
                        $.ajax({
                            type: "POST",
                            url: '{{route('reserv.note')}}',
                            data : {
                                _token: "{{ csrf_token() }}",
                                id : id,
                                note: comment_reserv,
                            },
                            dataType: "JSON",
                            success: function(result){
                                if(result.success == true){
                                    swal("ยกเลิกการจองห้องประชุมเรียบร้อยแล้ว!","","success")
                                    .then(function(){
                                        location.reload();
                                    });
                                }else{
                                    swal("Error!", "กรุณาลองใหม่อีกครั้ง!", "error");
                                }
                            }
                        });
                        return true;
                    }
                }
		    });
        }
    </script>
@endsection
