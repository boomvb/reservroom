<ul class="nav nav-tabs" id="myTabs" role="tablist">
    <li role="presentation" class="{{ Request::is('reservs/view/today') ? 'active' : '' }}">
        <a href="{{ url('reservs/view/today') }}" id="event_today-tab"aria-controls="event_today" aria-expanded="false"><i class="fa fa-calendar-minus-o" aria-hidden="true"></i>&nbsp;รายการจองวันนี้</a>
    </li>                                 
    <li role="presentation" class="{{ Request::is('reservs/view/calendar') ? 'active' : '' }}">
        <a href="{{ url('reservs/view/calendar') }}" id="event_list-tab" aria-controls="event_list" aria-expanded="false"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;ข้อมูลแบบปฏิทิน</a>
    </li> 
    <li role="presentation" class="{{ Request::is('reservs/view/table') ? 'active' : '' }}">
        <a href="{{ url('reservs/view/table') }}" id="event_table-tab" aria-expanded="false"><span class="glyphicon glyphicon-list"></span>&nbsp;ข้อมูลแบบตาราง</a>
    </li>
</ul>
