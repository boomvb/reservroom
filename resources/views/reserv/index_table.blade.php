@extends('layouts.layout')

@section('title', 'ระบบจองห้องประชุมสำนักงานอาสากาชาด')

@section('content')

@include('layouts.inc-menu')
@include('layouts.inc-minicontent-top')
@section('stylesheets')
    <link id="bsdp-css" href="{{ asset('css/datepicker.css') }}" rel="stylesheet">
@endsection
<div class="row">
	<div class="col-xs-12">
        <div class="page-header">
            <h1>รายการการใช้ห้องประชุม</h1>
        </div>
		<!--MessageAlert-->
        @include('flash_msg')
        <!--MessageAlert-->
        @include('reserv.inc-tab')
        <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade active in" role="tabpanel" id="event_today" aria-labelledby="event_today-tab">

                <form class="form-inline" action="{{ route('reserv.view', [$slug = 'table']) }}" method="get">
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="id_no">เลขที่จอง :</label>
                                <input type="text" name="id_no" id="id_no" value="{{ old('id_no') }}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="topic">หัวข้อประชุม :</label>
                                <input type="text" name="topic" id="topic" value="{{ old('topic') }}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="user">ชื่อผู้จอง :</label>
                                <select name="user" id="user" onchange="this.form.submit()">
                                    <option value="">--- กรุณาเลือก ---</option>
                                    @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->titles->name }} {{ $user->fullname }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="date_reserv">วันที่จอง :</label>
                                <input class="form-control date-picker" id="date_reserv" name="date_reserv" type="text" data-date-format="dd-mm-yyyy" value="{{ old('date_reserv') }}" autocomplete="off" />
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="topic">สถานะ :</label>
                                <select name="status" id="status" onchange="this.form.submit()">
                                <option value="">--- กรุณาเลือก ---</option>
                                    @foreach($statuss as $status)
                                        <option value="{{ $status->id }}">{{ $status->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <button type="submit" id="btn_search_data" onclick="" class="btn btn-success"><span class="glyphicon glyphicon-search"></span>&nbsp;ค้นหา</button>
                        </div>
                    </div>
                </form>
                <hr>
                <div class="clearfix"></div>
                    @if(Auth::user()->roles_id == 1 || Auth::user()->roles_id == 2)
                    <button style="margin-bottom: 10px;display: none;" class="btn btn-danger delete_all" data-url="{{ url('myproductsDeleteAll') }}">ยืนยันการจอง</button>
                    @endif
                <table class="table table-striped table-bordered">
                    <tr>
                        @if(Auth::user()->roles_id == 1 || Auth::user()->roles_id == 2)
                        <th width="1%">
                            <div class="form-group">
                            <input type="checkbox" id="all_chk" class="minimal">
                            </div>
                        </th>
                        @endif
                        <th width="5%">เลขที่จอง</th>
                        <th width="5%">สถานะ</th>
                        <th width="15%">หัวข้อประชุม</th>
                        <th width="10%">วัน/เวลาที่เริ่มต้น</th>
                        <th width="10%">วัน/เวลาที่สิ้นสุด</th>
                        <th width="10%">ชื่อผู้จอง</th>
                        <th width="10%">วันที่จอง</th>
                        <th width="8%" align="center">&nbsp;</th>
                    </tr>
                    @if(count($reservs) > 0)
                        @foreach($reservs as $reserv)
                        <tr>
                            @if(Auth::user()->roles_id == 1 || Auth::user()->roles_id == 2)
                            <td>
                                <div class="form-group">
                                    @if($reserv->status_reservs_id == 3)

                                    @else
                                        <input type="checkbox" class="sub_chk" data-id="{{$reserv->id}}" @if($reserv->status_reservs_id == 3) style="display: none;" @endif name="subchk[]">
                                    @endif
                                </div>
                            </td>
                            @endif
                            <td>{{ $reserv->id }}</td>
                            <td><font color="@if($reserv->status->id == 1) #A0522D @elseif($reserv->status->id == 2) #5cb85c @elseif($reserv->status->id == 3) #d9534f @endif">
                                    @if($reserv->status->id == 1)
                                        <i class="fa fa-circle-o-notch" aria-hidden="true"></i>
                                    @elseif($reserv->status->id == 2)
                                        <i class="fa fa-check-square" aria-hidden="true"></i>
                                    @elseif($reserv->status->id == 3)
                                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                                    @endif
                                    {{ $reserv->status->name }}
                                </font></td>
                            <td>{{ $reserv->topic }}</td>
                            <td>{{ ConvertDate($reserv->startday) }} <br> {{ ConvertTime($reserv->starttime) }}&nbsp;น.</td>
                            <td>{{ ConvertDate($reserv->endday) }} <br> {{ ConvertTime($reserv->endtime) }}&nbsp;น.</td>
                            <td>{{ $reserv->user->titles->name }}{{ $reserv->user->fullname }} </td>
                            <td>@if($reserv->created_at != NULL) {{ ConvertDateCreate($reserv->created_at) }} @endif</td>
                            <td><center><button class="btn btn-app btn-info btn-xs" onclick="javascript:window.location.href='{{ route('reserv.show',$reserv->id) }}'">
                            <i class="ace-icon fa fa-info bigger-120"></i>ดูข้อมูล</button></center></center></td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">No Data</td>
                        </tr>
                    @endif
                </table>
                <p>ทั้งหมด {{ $reservs->total() }} รายการ</p>
                {{ $reservs->links() }}
                </div>
        </div>
	</div><!-- /.col -->
</div><!-- /.row -->

@include('layouts.inc-minicontent-bottom')
@endsection

@section('scripts')
    <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script>
	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})
    </script>
    <script>
        //AllSelected//
        $('#all_chk').on('click', function(e) {
            if($(this).is(':checked',true))
            {
                $(".delete_all").css('display', 'block');
                $(".sub_chk").prop('checked', true);
            } else {
                $(".sub_chk").prop('checked',false);
                $(".delete_all").css('display', 'none');
            }
        });
        //One Selected//
        $(".sub_chk").on('click', function(e) {
            if($(this).is(':checked',true))
            {
                var cnt = $('[name="subchk[]"]:checked').length;

                $(".delete_all").css('display', 'block');
            } else {
                var cnt = $('[name="subchk[]"]:checked').length;
                if(cnt < 1){
                $(".delete_all").css('display', 'none');
                }
            }
        });

        $('.delete_all').on('click', function(e) {
            var allVals = [];
            $(".sub_chk:checked").each(function() {
                allVals.push($(this).attr('data-id'));
            });
            if(allVals.length <=0)
            {
                alert("Please select row.");
            }else {
                swal({
                    title: "ยืนยันการอนุมัติการจองประชุม?",
                    text: "จำนวน "+ allVals.length + " รายการ",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            var join_selected_values = allVals.join(",");

                            $.ajax({
                                url: '{{ route("reserv.updateStatusAll") }}',
                                type: 'POST',
                                data:
                                    {
                                        _token: "{{ csrf_token() }}",
                                        ids : join_selected_values
                                    },success: function (data) {
                                    if(data.success == true){
                                        swal("ยืนยันการจองห้องเรียบร้อยแล้ว!","","success")
                                            .then(function(){
                                                location.reload();
                                            });
                                    }else {
                                        swal("Error!", "เลขที่จองนี้ ถูกยกเลิกไปแล้ว กรุณาลองใหม่อีกครั้ง!", "error")
                                            .then(function(){
                                                //location.reload();
                                            });
                                        console.log(data)
                                    }
                                },
                                error: function (data) {
                                    alert(data.responseText);
                                }
                            });

                        }
                    });
            }
            });

    </script>
@endsection
