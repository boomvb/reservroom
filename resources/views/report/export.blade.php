{{ $reports }}
<div style="text-align: center">
    <img style="text-align:center;" src="{{ $logo }}">
</div>
<h2 style="text-align:center;font-size:15pt;"><strong>รายงานการใช้ห้องประชุม สำนักงานอาสากาชาด</strong></h2><br>
<div style="text-align: center;font-size:13pt"><strong><ins>ประจำปี&nbsp;{{ ($str_year+543) }}</ins></strong></div><br><br>
    <table style="border:2px solid #000000;font-size:12pt;width: 100%" cellPadding="9">
        <tr style="border:1px solid #000000;font-size:12pt;background-color:#DCDCDC;">
            <td align="center" valign="middle" style="width: 10%;border:1px solid #000000;text-align:center;" rowspan="2">
                <strong>ห้องประชุม</strong>
            </td>
            <td style="width: 10%;border:1px solid #000000;text-align:center;" colspan="3">
                <strong>การใช้งานห้องประชุม</strong>
            </td>
        </tr>
        <tr style="border:1px solid #000000;font-size:12pt;background-color:#DCDCDC;">
            <td style="width: 10%;border:1px solid #000000;text-align:center;">ครั้ง</td>
            <td style="width: 10%;border:1px solid #000000;text-align:center;">ชั่วโมง</td>
            <td style="width: 10%;border:1px solid #000000;text-align:center;">นาที</td>
        </tr>

    </table>
