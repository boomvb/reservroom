@extends('layouts.layout')

@section('title', 'ระบบจองห้องประชุมสำนักงานอาสากาชาด')

@section('content')

@include('layouts.inc-menu')
@include('layouts.inc-minicontent-top')
<div class="row">
	<div class="col-xs-12">
        
        <div class="page-header">
            <h1>รายงาน</h1>
        </div>
		<!--MessageAlert-->
		@include('flash_msg')
		<!--MessageAlert-->
        <ul class="nav nav-tabs" id="myTabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#report_year" id="report_year-tab" role="tab" data-toggle="tab" aria-controls="report_year" aria-expanded="false"><i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;รายงานแบบรายปี</a>
            </li>
            <li role="presentation" class="">
                <a href="#report_month" role="tab" id="report_month-tab" data-toggle="tab" aria-controls="report_month" aria-expanded="false"><i class="fa fa-file-text"></i>&nbsp;รายงานแบบรายเดือน</a>
            </li> 
            <li role="presentation" class="">
                <a href="#report_other" role="tab" id="report_other-tab" data-toggle="tab" aria-controls="report_other" aria-expanded="false"><i class="fa fa-file-text"></i>&nbsp;รายงานแบบกำหนดเอง</a>
            </li>
            <li role="presentation" class="">
                <a href="#report_room" role="tab" id="report_room-tab" data-toggle="tab" aria-controls="report_room" aria-expended="false"><i class="fa fa-file-text"></i>&nbsp;รายงานตามห้อง</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade active in" role="tabpanel" id="report_year" aria-labelledby="report_year-tab">
                <div class="row">
                    <div class="col-md-12">
                        <center>
                        <form class="form-inline" action="{{ route('report.reportyear') }}" method="post" id="form-report-year">
                        {{ csrf_field() }}
                            <div class="form-group">
                                <label for="report_year">เลือกปีที่ต้องการ </label>
                                <select id="report_year_txt" name="report_year">
                                    <option value="">---</option>
                                    @php for($i=2570;$i>=2559;$i--){@endphp
                                    <option value="{{ ($i-543) }}">{{ $i }}</option>
                                    @php } @endphp   
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary" id="btn_reportYear">พิมพ์</button>
                        </form>
                        </center>
                    </div>
                </div>                                  
            </div>
        </div>

	</div><!-- /.col -->
</div><!-- /.row -->
@include('layouts.inc-minicontent-bottom')
@endsection