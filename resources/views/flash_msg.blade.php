<script src="{{ asset('js/sweetalert/sweetalert.all.js') }}"></script>
@if(session('success'))
    <script>
        Swal.fire({!! Session::pull('alert.config') !!});
    </script>
@endif

@if(session('error'))
    <script>
        Swal.fire({!! Session::pull('alert.config') !!});
    </script>
@endif

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif