<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'titles_id', 'fullname', 'group_users_id', 'email', 'password', 'roles_id', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Titles()
    {
        return $this->belongsTo(Titles::Class, 'titles_id');
    }

    public function Group()
    {
        return $this->belongsTo(GroupUsers::Class, 'group_users_id');
    }

    public function Role()
    {
        return $this->belongsTo(Roles::Class, 'roles_id');
    }   

    protected function routeNotificationForLine()
    {
       return 'FQrUgIoZqSgWruTPCrI9iJVM72IchWPoiolt5kyZjqN';
    }
}
