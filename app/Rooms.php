<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rooms extends Model
{
    public $incrementing = false;

    protected $table = 'rooms';

    protected $fillable = [
        'id',
        'name',
        'desc',
        'num',
        'color',
        'images',
        'status_rooms_id',
        'children_code',
    ];

    public function StatusRoom()
    {
        return $this->belongsTo(StatusRooms::Class, 'status_rooms_id');
    }

    public function parent() {
        return $this->belongsTo(self::class, 'children_code');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children() {
        return $this->hasMany(Rooms::class, 'children_code');
    }
}
