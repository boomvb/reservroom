<?php

namespace App\Http\Controllers;

use App\ChildrenRooms;
use Illuminate\Http\Request;
use App\StatusRooms;
use App\Rooms;
use Storage;

class RoomController extends Controller
{
    protected $imagePath = 'images';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Rooms::with('StatusRoom')->get();

        return view('room.index', compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $chidrenrooms = Rooms::where('children_code', '=', '0')->get();
        $status = StatusRooms::get();

        return view('room.create', compact('status', 'chidrenrooms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'num' => 'required',
            'status' => 'required',
            'color' => 'required',
        ],[
            'name.required' => 'กรุณากรอกชื่อห้อง',
            'num.required' => 'กรุณากรอกจำนวนคนในห้อง',
            'status.required' => 'กรุณาเลือกสถานะของห้อง',
            'color.required' => 'กรุณาเลือกสีของห้อง',
        ]);
        $uploadImages = null;
        try {
            $name = $request->get('name');
            $desc = $request->get('desc');
            $num = $request->get('num');
            $status = $request->get('status');
            $color = $request->get('color');
            $img = $request->get('img');
            $children_code = $request->get('children_code');
            //GetId//
            //return $children_code;
            $genId = $this->getId();

            $rooms = new Rooms;
            $rooms->id = $genId;
            $rooms->name = $name;
            $rooms->desc = $desc;
            $rooms->num = $num;
            $rooms->status_rooms_id = $status;
            $rooms->color = $color;
            $rooms->children_code  = $children_code;
            if($request->file('images') != null){
                $uploadImages = Storage::disk('storage')->put($this->imagePath, $img);
                $rooms->images = $uploadImages;
            }else{
                $rooms->images = "";
            }
            $rooms->save();
            //return $rooms;

            return redirect()->route('room.view')->with('success', 'บันทึกข้อมูลเรียบร้อยแล้ว');
        } catch (\Exception $exception) {
            if ($uploadImages != null) {
                Storage::disk('storage')->delete($this->imagePath . '/' . $img);
            }
            return redirect()->route('room.create')->with('error', $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $rooms = Rooms::find($id);
            $chidrenrooms = Rooms::where('children_code', '=', '0')->get();
            $status = StatusRooms::get();

            return view('room.edit', compact('rooms', 'status', 'chidrenrooms'));
        } catch (\Exception $exception) {
            return redirect()->route('room.create')->with('error', $exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'num' => 'required',
            'status' => 'required',
            'color' => 'required',
        ],[
            'name.required' => 'กรุณากรอกชื่อห้อง',
            'num.required' => 'กรุณากรอกจำนวนคนในห้อง',
            'status.required' => 'กรุณาเลือกสถานะของห้อง',
            'color.required' => 'กรุณาเลือกสีของห้อง',
        ]);
        $uploadImages = null;
        try {
            $id = $request->get('id_rooms');
            $name = $request->get('name');
            $desc = $request->get('desc');
            $num = $request->get('num');
            $status = $request->get('status');
            $color = $request->get('color');
            $img = $request->get('img');
            $children_code = $request->get('children_code');

            $rooms = Rooms::find($id);
            $rooms->id = $id;
            $rooms->name = $name;
            $rooms->desc = $desc;
            $rooms->num = $num;
            $rooms->status_rooms_id = $status;
            $rooms->color = $color;
            $rooms->children_code  = $children_code;
            if($request->file('images') != null){
                $uploadImages = Storage::disk('storage')->put($this->imagePath, $img);
                $rooms->images = $uploadImages;
            }
            $rooms->save();

            return redirect()->route('room.view')->with('success', 'แก้ไขข้อมูลเรียบร้อยแล้ว');
        } catch (\Exception $exception) {
            if ($uploadImages != null) {
                Storage::disk('storage')->delete($this->imagePath . '/' . $img);
            }
            return redirect()->route('room.create')->with('error', $exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room = Rooms::find($id);
        $room->delete();
        return redirect()->route('room.view')->with('success', 'ลบข้อมูลเรียบร้อยแล้ว');

    }


    public function getId()
    {
        $gen = Rooms::max('id');
        // Split string and number
        $numberLastid = substr($gen, 6);
        // Convert string number to integer
        $insertid = intval($numberLastid);
        // Zero-padded integer
        $stringInsertid = 'RVB' . sprintf("%05d", $insertid+1);

        return $stringInsertid;
    }
}
