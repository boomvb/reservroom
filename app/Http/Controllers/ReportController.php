<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reserv;
use App\Rooms;
use PDF;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('report.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function report_year(Request $request)
    {
        try {
            $str_year = $request->get('report_year');
            
            $str_startday = ($str_year-1)."-10-01";
            $str_endday = $str_year."-09-30";
            
            $reports = Reserv::with('room')
                ->where('status_reservs_id', 2)
                ->orWhere('startday', '>=', $str_startday)
                ->where('endday', '<=', $str_endday)
                ->orWhere('endday', '>=', $str_startday)
                ->where('endday', '<=', $str_endday)
                ->whereHas('room', function($q){
                    $q->groupBy('name');
                })->get();
            $cnt_reports = $reports->count();
            if($cnt_reports >= 1){
                $data = [
                    'logo' => asset('public/images/logo_vb.png'),
                    'reports' => $reports,
                ];
                $pdf = PDF::loadView('report.export', $data);
                return @$pdf->stream();
            }else{
                return redirect()->route('report.view')->with('error', 'ไม่มีข้อมูลการรายงาน');
            }

        } catch (\Exception $exception) {
            //throw $th;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function report_month(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function report_mannul(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
