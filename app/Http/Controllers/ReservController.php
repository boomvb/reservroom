<?php

namespace App\Http\Controllers;

use App\RoomReservs;
use Illuminate\Http\Request;
use App\Rooms;
use App\TypesReserv;
use App\PositionReservs;
use App\Reserv;
use App\User;
use Auth;
use Intervention\Image\Facades\Image;
use PDF;
use Calendar;
use App\StatusReservs;
use App\Notifications\ReservNotification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ReservController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $slug)
    {
        if($slug == 'today'){
            $rooms = Rooms::get();

            return view('reserv.index_today', compact('rooms'));
        }else if($slug == 'calendar'){

            return view('reserv.index_calendar');
        }else if($slug == 'table'){

            $users = User::get();
            $statuss = StatusReservs::get();

            if($request->get('id_no') != ""){
                $reservs = Reserv::with('status', 'user')->where('id', 'LIKE', '%'.$request->get('id_no').'%')->paginate(20);
            }else if($request->get('topic') != ""){
                $reservs = Reserv::with('status', 'user')->where('topic', 'LIKE', '%'.$request->get('topic').'%')->paginate(20);
            }else if($request->get('user') != ""){
                $reservs = Reserv::with('status', 'user')->where('user_id', $request->get('user'))->paginate(20);
            }else if($request->get('date_reserv') != ""){
                $reservs = Reserv::with('status', 'user')->where('created_at', 'LIKE', '%'.$request->get('date_reserv').'%')->paginate(20);
            }else if($request->get('status') != ""){
                $reservs = Reserv::with('status', 'user')->where('status_reservs_id', 'LIKE', '%'.$request->get('status').'%')->paginate(20);
            }else {
                $reservs = Reserv::with('status', 'user')->orderBy('id', 'DESC')->paginate(20);
            }

            return view('reserv.index_table', compact('reservs', 'users', 'statuss'))->with("reservs", $reservs);
        }
    }

    public function create()
    {
        $rooms = Rooms::where('children_code', '0')->get();
        $chidren_rooms = Rooms::where('children_code', '!=', '0')->get();
        $types = TypesReserv::get();
        $positions = PositionReservs::get();

        return view('reserv.create', compact('rooms', 'types', 'positions', 'chidren_rooms'));
    }

    public function store(Request $request)
    {
        Log::info($request);
        $this->validate($request,[
            'startday' => 'required',
            'startmonth' => 'required',
            'startyear' => 'required',
            'endday' => 'required',
            'endmonth' => 'required',
            'endyear' => 'required',
            'starttime' => 'required',
            'endtime' => 'required',
            'room' => 'required',
            'typereserv' => 'required',
            'topic' => 'required',
            'num' => 'required',
            'tel' => 'required',
            'check_catering' => 'required',
            'position_reserv' => 'required'
        ],[
            'startday.required' => 'กรุณาเลือกวันที่เริ่มต้น',
            'startmonth.required' => 'กรุณาเลือกเดือนที่เริ่มต้น',
            'startyear.required' => 'กรุณาเลือกปีที่เริ่มต้น',
            'endday.required' => 'กรุณาเลือกวันที่สิ่้นสุด',
            'endmonth.required' => 'กรุณาเลือกเดือนที่สิ้นสุด',
            'endyear.required' => 'กรุณาเลือกปีที่สิ้นสุด',
            'starttime.required' => 'กรุณาเลือกเวลาที่เริ่มต้น',
            'endtime.required' => 'กรุณาเลือกเวลาที่สิ้นสุด',
            'room.required' => 'กรุณาเลือกห้องประชุม',
            'typereserv.required' => 'กรุณาเลือกประเภทของการประชุม',
            'topic.required' => 'กรุณากรอกหัวข้อการประชุม',
            'num.required' => 'กรุณากรอกจำนวนผู้เข้าประชุม',
            'tel.required' => 'กรุณากรอกเบอร์ติดต่อ',
            'check_catering.required' => 'กรุณาเลือกอุปกรณ์จัดอาหาร',
            'position_reserv.required' => 'กรุณาเลือกรูปแบบการจัดห้องประชุม'
        ]);
        try {
            $str_startday = $request->get('startday');
            $str_startmonth = $request->get('startmonth');
            $str_startyear = $request->get('startyear');
            $convert_startdate = ($str_startyear-543)."-".$str_startmonth."-".$str_startday;
            $str_starttime = $request->get('starttime');

            $str_endday = $request->get('endday');
            $str_endmonth = $request->get('endmonth');
            $str_endyear = $request->get('endyear');
            $convert_enddate = ($str_endyear-543)."-".$str_endmonth."-".$str_endday;

            $str_endtime = $request->get('endtime');

            $str_room = $request->get('room');
            $str_children_room = $request->get('ch_children_room');

            $str_typereserv = $request->get('typereserv');
            $str_topic = $request->get('topic');
            $str_desc = $request->get('desc');
            $str_num = $request->get('num');
            $str_namejoin = $request->get('namejoin');
            $str_tel = $request->get('tel');
            $str_check_catering = $request->get('check_catering');
            $str_txt_catering2 = $request->get('txt_catering2');
            $str_txt_cateringother = $request->get('txt_cateringother');
            $str_check_projector = $request->get('check_projector');
            $str_check_screen = $request->get('check_screen');
            $str_check_dvd = $request->get('check_dvd');
            $str_check_tv = $request->get('check_tv');
            $str_check_record = $request->get('check_record');
            $str_check_amp = $request->get('check_amp');
            $str_check_control = $request->get('check_control');
            $str_txt_control = $request->get('txt_control');
            $str_check_wireless_mic = $request->get('check_wireless_mic');
            $str_txt_wireless_mic = $request->get('txt_wireless_mic');
            $str_check_mic = $request->get('check_mic');
            $str_txt_mic = $request->get('txt_mic');
            $str_check_other = $request->get('check_other');
            $str_txt_other = $request->get('txt_other');
            $str_position_reserv = $request->get('position_reserv');
            $str_txt_table_reserv = $request->get('txt_tableReserv');

            Log::info('reserv user: '.$request);
            //CheckRoomซ้ำวันจอง//

            $children_room = '';
            if(is_array($str_children_room))
                $children_room = implode(',', $str_children_room);

            $check_reserv = DB::select("
            SELECT *
                FROM reservs
                WHERE rooms_id IN
                (
                 SELECT
                  rooms.id
                 FROM
                  rooms
                  LEFT OUTER JOIN
                    room_reservs on reservs.id = room_reservs.reservs_id
                 WHERE
                        (startday BETWEEN '".$convert_startdate."' AND '".$convert_enddate."')
                        OR
                        (endday BETWEEN '".$convert_startdate."' AND '".$convert_enddate."')
                        OR
                        ('".$convert_startdate."' BETWEEN startday AND endday)
                        OR
                        ('".$convert_enddate."' BETWEEN startday AND endday)
                  AND
                  (
                        (starttime BETWEEN '".$str_starttime."' AND '".$str_endtime."')
                        OR
                        (endtime BETWEEN '".$str_starttime."' AND '".$str_endtime."')
                        OR
                        ('".$str_starttime."' BETWEEN starttime AND endtime)
                        OR
                        ('".$str_endtime."' BETWEEN starttime AND endtime)
                  )
                  AND room_reservs.room_id IN ('".$children_room."')
                )
                AND rooms_id = '".$str_room."'
                AND status_reservs_id != '3'
            ");

            $cnt_reserv = count($check_reserv);
            if($cnt_reserv >= 1){
                Log::info($check_reserv);
                return redirect()->route('reserv.create')->with('error','ไม่สามารถจองห้องได้ เนื่องจากห้องถูกจองใช้งานไปแล้ว กรุณาเลือกวัน/เวลาจองอื่น');
            }else{
                //GenId//
                $genId = $this->genCode();

                $reserv = new Reserv;
                $reserv->id = $genId;
                $reserv->startday = $convert_startdate;
                $reserv->endday = $convert_enddate;
                $reserv->starttime = $str_starttime;
                $reserv->endtime = $str_endtime;
                $reserv->rooms_id = $str_room;
                $reserv->types_id = $str_typereserv;
                $reserv->topic = $str_topic;
                $reserv->desc = $str_desc;
                $reserv->num = $str_num;
                $reserv->namejoin = $str_namejoin;
                $reserv->tel = $str_tel;
                $reserv->check_catering = $str_check_catering;
                $reserv->txt_catering2 = $str_txt_catering2;
                $reserv->txt_cateringother = $str_txt_cateringother;
                $reserv->check_projector = $str_check_projector;
                $reserv->check_screen = $str_check_screen;
                $reserv->check_dvd = $str_check_dvd;
                $reserv->check_tv = $str_check_tv;
                $reserv->check_record = $str_check_record;
                $reserv->check_amp = $str_check_amp;
                $reserv->check_control = $str_check_control;
                $reserv->txt_control = $str_txt_control;
                $reserv->check_wireless_mic = $str_check_wireless_mic;
                $reserv->txt_wireless_mic = $str_txt_wireless_mic;
                $reserv->check_mic = $str_check_mic;
                $reserv->txt_mic = $str_txt_mic;
                $reserv->check_other = $str_check_other;
                $reserv->txt_other = $str_txt_other;
                $reserv->status_reservs_id = 1;
                $reserv->position_reservs_id = $str_position_reserv;
                $reserv->user_id = Auth::user()->id;
                $reserv->note_position_reserv = $str_txt_table_reserv;
                $reserv->save();

                if (is_array($str_children_room)){
                    foreach ($str_children_room as $key => $item) {
                        RoomReservs::create(
                            [
                                'room_id' => $item,
                                'reservs_id' => $reserv->id,
                            ]);
                    }
                }

                return redirect()->route('reserv.view', ['slug' => 'table'])->with('success','จองห้องประชุมเรียบร้อยแล้ว');
            }

        } catch (\Exception $exception) {
            Log::alert($exception->getMessage());
            return $exception->getMessage();
        }
    }
    public function show($id)
    {
        try {
            $logo  = asset('images/logo_vb.png');
            $reservs = Reserv::with('RoomsReserv')->find($id);

            return view('reserv.show', compact('reservs', 'logo'));
        } catch (\Exception $exception) {
            return redirect()->route('reserv.view', ['slug' => 'table'])->with('error', $exception->getMessage());
        }
    }

    public function edit($id)
    {
        try {
            $reservs = Reserv::find($id);
            $rooms = Rooms::where('children_code', '0')->get();
            $chidren_rooms = Rooms::where('children_code', '!=', '0')->get();
            $types = TypesReserv::get();
            $positions = PositionReservs::get();

            return view('reserv.edit', compact('reservs', 'rooms', 'types', 'positions', 'chidren_rooms'));
        } catch (\Exception $exception) {
            return redirect()->route('reserv.view', ['slug' => 'table'])->with('error', $exception->getMessage());
        }
    }

    public function update(Request $request)
    {
        try {
            $str_id = $request->get('id_reserv');
            $str_startday = $request->get('startday');
            $str_startmonth = $request->get('startmonth');
            $str_startyear = $request->get('startyear');

            $convert_startdate = ($str_startyear-543)."-".$str_startmonth."-".$str_startday;
            $str_starttime = $request->get('starttime');

            $str_endday = $request->get('endday');
            $str_endmonth = $request->get('endmonth');
            $str_endyear = $request->get('endyear');
            $convert_enddate = ($str_endyear-543)."-".$str_endmonth."-".$str_endday;

            $str_endtime = $request->get('endtime');

            $str_room = $request->get('room');
            $str_typereserv = $request->get('typereserv');
            $str_topic = $request->get('topic');
            $str_desc = $request->get('desc');
            $str_num = $request->get('num');
            $str_namejoin = $request->get('namejoin');
            $str_tel = $request->get('tel');
            $str_check_catering = $request->get('check_catering');
            $str_txt_catering2 = $request->get('txt_catering2');
            $str_txt_cateringother = $request->get('txt_cateringother');
            $str_check_projector = $request->get('check_projector');
            $str_check_screen = $request->get('check_screen');
            $str_check_dvd = $request->get('check_dvd');
            $str_check_tv = $request->get('check_tv');
            $str_check_record = $request->get('check_record');
            $str_check_amp = $request->get('check_amp');
            $str_check_control = $request->get('check_control');
            $str_txt_control = $request->get('txt_control');
            $str_check_wireless_mic = $request->get('check_wireless_mic');
            $str_txt_wireless_mic = $request->get('txt_wireless_mic');
            $str_check_mic = $request->get('check_mic');
            $str_txt_mic = $request->get('txt_mic');
            $str_check_other = $request->get('check_other');
            $str_txt_other = $request->get('txt_other');
            $str_position_reserv = $request->get('position_reserv');
            $str_txt_table_reserv = $request->get('txt_tableReserv');

            $str_status_id = $request->get('id_status');

                $reserv = Reserv::find($str_id);
                $reserv->id = $str_id;
                $reserv->startday = $convert_startdate;
                $reserv->endday = $convert_enddate;
                $reserv->starttime = $str_starttime;
                $reserv->endtime = $str_endtime;
                $reserv->rooms_id = $str_room;
                $reserv->types_id = $str_typereserv;
                $reserv->topic = $str_topic;
                $reserv->desc = $str_desc;
                $reserv->num = $str_num;
                $reserv->namejoin = $str_namejoin;
                $reserv->tel = $str_tel;
                $reserv->check_catering = $str_check_catering;
                $reserv->txt_catering2 = $str_txt_catering2;
                $reserv->txt_cateringother = $str_txt_cateringother;
                $reserv->check_projector = $str_check_projector;
                $reserv->check_screen = $str_check_screen;
                $reserv->check_dvd = $str_check_dvd;
                $reserv->check_tv = $str_check_tv;
                $reserv->check_record = $str_check_record;
                $reserv->check_amp = $str_check_amp;
                $reserv->check_control = $str_check_control;
                $reserv->txt_control = $str_txt_control;
                $reserv->check_wireless_mic = $str_check_wireless_mic;
                $reserv->txt_wireless_mic = $str_txt_wireless_mic;
                $reserv->check_mic = $str_check_mic;
                $reserv->txt_mic = $str_txt_mic;
                $reserv->check_other = $str_check_other;
                $reserv->txt_other = $str_txt_other;
                $reserv->status_reservs_id = $str_status_id;
                $reserv->position_reservs_id = $str_position_reserv;
                $reserv->user_id = Auth::user()->id;
                $reserv->note_position_reserv = $str_txt_table_reserv;
                $reserv->save();


                return redirect()->route('reserv.view', ['slug' => 'table'])->with('success','แก้ไขข้อมูลการจองเรียบร้อยแล้ว');

        } catch (\Exception $exception) {
            return redirect()->route('reserv.view', ['slug' => 'table'])->with('error', $exception->getMessage());
        }
    }

    public function exportPDF($id)
    {
        try{
            $reserv  = Reserv::with('status', 'user', 'RoomsReserv')->find($id);
            $data = [
                'logo' => asset('images/logo_vb.png'),
                'reservs' => $reserv,
            ];

            $pdf = PDF::loadView('reserv.export', $data);
            //return @$pdf->stream();
            return $pdf->download($id."_".$reserv->topic.".pdf");
        }catch (Exception $exception){
            return redirect()->route('reserv.view', ['slug' => 'table'])->with('error', $exception->getMessage());
        }
    }
    public function updateStatus(Request $request)
    {
        try {
            $reservs = Reserv::find($request->get('id'));
            $reservs->id = $request->get('id');
            $reservs->status_reservs_id = 2;
            $reservs->save();

            return response()->json(['success' => true]);
        } catch (\Exception $exception) {
            return redirect()->route('reserv.view', ['slug' => 'table'])->with('error', $exception->getMessage());
        }
    }

    public function updateStatusAll(Request $request)
    {
        try {
                $ids = $request->get('ids');
                Reserv::whereIn('id',explode(",",$ids))->update(array('status_reservs_id' => 2));
                return response()->json(['success'=> true]);

        }catch (\Exception $exception){
            return redirect()->route('reserv.view', ['slug' => 'table'])->with('error', $exception->getMessage());
        }
    }

    public function storeNote(Request $request)
    {
        try {
            $reservs = Reserv::find($request->get('id'));
            $reservs->id = $request->get('id');
            $reservs->note = $request->get('note');
            $reservs->status_reservs_id = 3;
            $reservs->save();

            return response()->json(['success' => true]);
        } catch (\Exception $exception) {
            return redirect()->route('reserv.view', ['slug' => 'table'])->with('error', $exception->getMessage());
        }
    }
    public function genCode()
    {
        $gen = Reserv::max('id');
        // Split string and number
        $numberLastid = substr($gen, 5);
        // Convert string number to integer
        $insertid = intval($numberLastid);
        // Zero-padded integer
        $stringInsertid = 'VBR' . sprintf("%05d", $insertid+1);

        return $stringInsertid;
    }

    public function GetEvents()
    {
        $data = Reserv::with('status', 'user', 'RoomsReserv')->where('status_reservs_id', '!=', 3)->get();
        $count = $data->count();
        if($count >= 1){
            foreach($data as $events) {

                if($events->rooms_id == "RVB00001"){
                    $className = "room1";
                }
                if($events->rooms_id == "RVB00002"){
                    $className = "room2";
                }
                if($events->rooms_id == "RVB00003"){
                    $className = "room3";
                }
                if($events->rooms_id == "RVB00004"){
                    $className = "room4";
                }
                if($events->rooms_id == "RVB00005"){
                    $className = "room5";
                }
                if($events->rooms_id == "RVB00006"){
                    $className = "room6";
                }
                if($events->rooms_id == "RVB00007"){
                    $className = "room7";
                }
                if($events->rooms_id == "RVB00008"){
                    $className = "room8";
                }
                if($events->rooms_id == 0) {
                    $className = '';
                    $nameRoom = '';
                    $colorRoom = '';
                }else{
                    $nameRoom = $events->room->name;
                    $colorRoom = $events->room->color;
                }



                $data_events[] = array(
                    'id' => $events->id,
                    'title' => $events->topic,
                    'desc' => $events->desc,
                    'start' => $events->startday." ".$events->starttime,
                    'end' => $events->endday." ".$events->endtime,
                    'className' => $className,
                    'url' => '../show/'.$events->id,
                    'id_room' => $events->rooms_id,
                    'room' => $nameRoom,
                    'childrenroom' => $events->RoomsReserv,
                    'name' => $events->user->titles->name."".$events->user->fullname,
                    'status' => $events->status->name,
                    'color' => $events->room->color,
                );

            }

            foreach ($events->RoomsReserv as $roomreserv) {
                $data_events[]['childrenroom'] = $roomreserv->room->name;
            }

            return response()->json($data_events);
        }
    }

    public function GetChildrenRooms(Request $request)
    {
        $chidren_rooms = Rooms::where('children_code', '!=', '0')->where('children_code', $request->get('room_id'))->get();

        return response()->json(['children_rooms' => $chidren_rooms]);
    }
}
