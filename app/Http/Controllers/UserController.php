<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Reserv;
use App\User;
use App\Titles;
use App\GroupUsers;
use App\Roles;
use App\Hasroles;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       try {
           $users = User::with('titles', 'group', 'role')->orderBy('id', 'asc')->paginate(20);
 
           return view('user.index', compact('users'));

       } catch (\Exception $exception) {
            return redirect()->route('user.view')->with('error', $exception->getMessage());
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titles = Titles::get();
        $groupusers = GroupUsers::get();
        $roles = Roles::get();

        return view('user.create', compact('titles', 'groupusers', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'username' => 'required',
            'password' => 'required',
            'name' => 'required',
            'email' => 'required',
            'group_user' => 'required',
            'role' => 'required',
        ],[
            'title.required' => 'กรุณาระบุคำนำหน้าชื่อด้วยครับ',
            'username.required' => 'กรุณาระบุชื่อผู้ใช้งานด้วยครับ',
            'password.required' => 'กรุณาระบุรหัสผ่านด้วยครับ',
            'name.required' => 'กรุณาระบุชื่อ-นามสกุลด้วยครับ',
            'email.required' => 'กรุณาระบุอีเมล์ด้วยครับ',
            'group_user.required' => 'กรุณาระบุฝ่ายงานด้วยครับ',
            'role.required' => 'กรุณาระบุสิทธิ์ผู้ใช้งานด้วยครับ'
        ]);

        try {
            $title = $request->get('title');
            $name = $request->get('name');
            $username = $request->get('username');
            $password = $request->get('password');
            $email = $request->get('email');
            $group_user = $request->get('group_user');
            $role = $request->get('role');
            //CheckUserซ้ำ//
            $checkUser = User::where('username', '=', $username)->count();
            if($checkUser == 1){
                return redirect()->route('user.view')->with('error', 'มีชื่อผู้ใช้งานอยู่ระบบแล้ว กรุณาเปลี่ยนเป็นชื่อผู้ใช้งานอื่น');
            }else{
                $user = new User;
                $user->username = $username;
                $user->titles_id = $title;
                $user->fullname = $name;
                $user->group_users_id = $group_user;
                $user->email = $email;
                $user->password = Hash::make($password);
                $user->roles_id = $role;
                $user->active = 1;
                $user->save();

                return redirect()->route('user.view')->with('success', 'บันทึกข้อมูลเรียบร้อยแล้ว');
            }
            
        } catch (\Exception $exception) {

            return redirect()->route('user.view')->with('error', $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $edituser = User::with(['Group','Role','Titles'])->find($id);
            $titles = Titles::get();
            $groupusers = GroupUsers::get();
            $roles = Roles::get();

            return view('user.edit',compact('edituser','titles','groupusers','roles'));
        } catch (\Exception $exception) {
            return redirect()->route('user.view')->with('error', $exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'username' => 'required',
            'name' => 'required',
            'email' => 'required',
            'group_user' => 'required',
            'role' => 'required',
        ],[
            'title.required' => 'กรุณาระบุคำนำหน้าชื่อด้วยครับ',
            'username.required' => 'กรุณาระบุชื่อผู้ใช้งานด้วยครับ',
            'name.required' => 'กรุณาระบุชื่อ-นามสกุลด้วยครับ',
            'email.required' => 'กรุณาระบุอีเมล์ด้วยครับ',
            'group_user.required' => 'กรุณาระบุฝ่ายงานด้วยครับ',
            'role.required' => 'กรุณาระบุสิทธิ์ผู้ใช้งานด้วยครับ'
        ]);

        try {
            $user = User::find($id);
            if(!$user)
            {
                return trans('app.invalid_request');
            }
            $user->titles_id = $request->input('title');
            $user->username = $request->input('username');
            if($request->input('new_password') != $request->input('confirm_password'))
            {
                if(empty($request->input('confirm_password')))
                {
                    return redirect()->back()->with('error', 'กรุณายืนยันรหัสผ่านด้วย');
                }
                return redirect()->back()->with('error', 'กรุณาระบุรหัสผ่านให้ตรงกัน');
            }
            if(!empty($request->input('new_password'))){
                $user->password = Hash::make($request->input('new_password'));
            }
            $user->fullname = $request->input('name');
            $user->email = $request->input('email');
            $user->group_users_id = $request->input('group_user');
            $user->roles_id = $request->input('role');
            $user->save();

            return redirect()->route('user.view')->with('success', 'แก้ไขข้อมูลผู้ใช้งานเรียบร้อยแล้ว');

        } catch (\Exception $exception) {
            return redirect()->route('user.view')->with('error', $exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function enabled(Request $request)
    {
        try{
            $user = User::find($request->get('id'));
            $user->active = 0;
            $user->save();
            
            return response()->json(['status' => true]);
        }catch(\Exception $exception){
            return redirect()->route('user.view')->with('error', $exception->getMessage());
        }
    }


    public function disabled(Request $request)
    {
        try{
            $user = User::find($request->get('id'));
            $user->active = 1;
            $user->save();
            
            return response()->json(['status' => true]);
        }catch(\Exception $exception){
            
            return redirect()->route('user.view')->with('error', $exception->getMessage());
        }
    }


    public function history($id)
    {
        try {
            $history = Reserv::with('status', 'user', 'room')->where('user_id', Auth::user()->id)->orderBy('id', 'asc')->paginate(10);

            return view('user.history', compact('history'));
        } catch (\Exception $exception) {

            return redirect()->route('user.view')->with('error', $exception->getMessage());
        }
    }

    public function editProfile($id)
    {
        try {
            $titles = Titles::get();
            $groupusers = GroupUsers::get();

            $profiles = User::find($id);

            return view('user.profile', compact('profiles', 'titles', 'groupusers'));
        } catch (\Exception $exception) {

            return redirect()->route('user.view')->with('error', $exception->getMessage());
        }
    }

    public function updateProfile(Request $request,$id)
    {
        try {
            $user = User::find($id);
            if(!$user)
            {
                return trans('app.invalid_request');
            }
            $user->titles_id = $request->input('title');
            $user->username = $request->input('username');
            if($request->input('new_password') != $request->input('confirm_password'))
                {
                    if(empty($request->input('confirm_password')))
                    {
                        return redirect()->back()->with('error', 'กรุณายืนยันรหัสผ่านด้วย');
                    }
                    return redirect()->back()->with('error', 'กรุณาระบุรหัสผ่านให้ตรงกัน');
                }
            if(!empty($request->input('new_password'))){
                $user->password = Hash::make($request->input('new_password'));
            }
            $user->fullname = $request->input('name');
            $user->email = $request->input('email');
            $user->group_users_id = $request->input('groupuser');
            $user->save();

            return redirect()->back()->with('success', 'แก้ไขข้อมูลส่วนตัวเรียบร้อยแล้ว');
        } catch (\Exception $exception) {
            
            return redirect()->route('user.view')->with('error', $exception->getMessage());
        }
    }

    public function setPassword($id)
    {
        try{
            $user = User::find($id);
            if(!$user)
            {
                return trans('app.invalid_request');
            }
            $user->password = Hash::make('123456');
            $user->save();

            return redirect()->back()->with('success', 'ตั้งรหัสผ่าน 123456 เรียบร้อยแล้ว');
        } catch(\Exception $exception) {
            return redirect()->route('user.view')->with('error', $exception->getMessage());
        }
    }
}
