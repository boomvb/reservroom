<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Hinaloe\LineNotify\Message\LineMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Session;

class LoginNotification extends Notification
{
    use Queueable;
    protected $login;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $login)
    {
        $this->login  = $login;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['line'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toLine($notifable)
        {
            return (new LineMessage())->message('Login authencation:
SSID: '.Session::getId().'
IP: '.$_SERVER['REMOTE_ADDR'].'
Name: '.$this->login->titles->name.$this->login->fullname.'
Date: '.date('d-m-Y H:i:s')
            );
        }    
}
