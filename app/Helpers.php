<?php
function ConvertDate($value){
    $thai_month_arr=array(
        "00"=>"",
        "01"=>"มกราคม",
        "02"=>"กุมภาพันธ์",
        "03"=>"มีนาคม",
        "04"=>"เมษายน",
        "05"=>"พฤษภาคม",
        "06"=>"มิถุนายน", 
        "07"=>"กรกฎาคม",
        "08"=>"สิงหาคม",
        "09"=>"กันยายน",
        "10"=>"ตุลาคม",
        "11"=>"พฤศจิกายน",
        "12"=>"ธันวาคม"                 
    );
    //2019-06-25
    $ex_str = explode("-",$value);
    $convert_strmonth = $ex_str[1];
    $convert_strmonth = $thai_month_arr[$convert_strmonth];
    $newyear = $ex_str[0]+543;
    $newStr = $ex_str[2]."-".$convert_strmonth."-".$newyear;

    return $newStr;
}

function ConvertDay($value){
    //Day30//
    $ConvertDate = ConvertDate($value);
    $ex_date = explode("-",$ConvertDate);
    $newStrDay = $ex_date[0];

    return $newStrDay;
}

function ConvertMonth($value){

    $thai_month_arr=array(
        "00"=>"",
        "01"=>"มกราคม",
        "02"=>"กุมภาพันธ์",
        "03"=>"มีนาคม",
        "04"=>"เมษายน",
        "05"=>"พฤษภาคม",
        "06"=>"มิถุนายน", 
        "07"=>"กรกฎาคม",
        "08"=>"สิงหาคม",
        "09"=>"กันยายน",
        "10"=>"ตุลาคม",
        "11"=>"พฤศจิกายน",
        "12"=>"ธันวาคม"                 
    );
    //Month12//
    $ConvertDate = ConvertDate($value);
    $ex_date = explode("-",$ConvertDate);
    $newStrMonth = $ex_date[1];

    return $newStrMonth;
}

function ConvertYear($value){
    //Year543//
    $ConvertDate = ConvertDate($value);
    $ex_date = explode("-",$ConvertDate);
    $newStrYear = $ex_date[2];

    return $newStrYear;
}

function ConvertTime($value){
    //Time 08:00:00 //
    $newTime = substr($value,0,-3);

    return $newTime;
}

function ConvertDateCreate($value){
    //Date2018-09-12 15:34:34//
    $ex_str = explode(" ",$value);
    $newStr = ConvertDate($ex_str[0]);

    return $newStr;
}

function ConvertDayCreate($value){
    //Date2018-09-12 15:34:34//
    $ConvertDateCreate = ConvertDateCreate($value);
    $ex_date = explode("-",$ConvertDateCreate);
    $newStrDay = $ex_date[0];

    return $newStrDay;
}

function ConvertYearCreate($value){
    //Date2018-09-12 15:34:34//
    $ConvertDateCreate = ConvertDateCreate($value);
    $ex_date = explode("-",$ConvertDateCreate);
    $newStrYear = $ex_date[2];

    return $newStrYear;
}


//RoleBase//
function checkPermission($permissions){
    $userAccess = getMyPermission(auth()->user()->roles_id);
    foreach ($permissions as $key => $value) {
      if($value == $userAccess){
        return true;
      }
    }
    return false;
  }


function getMyPermission($id){
    switch ($id) {
      case 1:
        return 'administrator';
        break;
      case 2:
        return 'officer';
        break;
      default:
        return 'user';
        break;
    }
}