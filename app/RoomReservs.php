<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomReservs extends Model
{
   protected $table = 'room_reservs';
   protected $fillable = [
       'room_id',
       'reservs_id'
   ];

   public function Room()
   {
       return $this->belongsTo(Rooms::class, 'room_id');
   }
}
