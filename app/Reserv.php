<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Reserv extends Model
{
    use Notifiable;

    public $incrementing = false;
    protected $table = "reservs";
    protected $fillable = [
        'startday',
        'endday',
        'starttime',
        'endtime',
        'rooms_id',
        'types_id',
        'topic',
        'desc',
        'num',
        'namejoin',
        'tel',
        'check_catering',
        'txt_catering2',
        'txt_cateringother',
        'check_projector',
        'check_screen',
        'check_dvd',
        'check_tv',
        'check_record',
        'check_amp',
        'check_control',
        'txt_control',
        'check_wireless_mic',
        'txt_wireless_mic',
        'check_mic',
        'txt_mic',
        'check_other',
        'txt_other',
        'status_reservs_id',
        'create_date',
        'position_reservs_id',
        'user_id',
        'note',
        'note_position_reserv'
    ];


    public function Status()
    {
        return $this->belongsTo(StatusReservs::Class, 'status_reservs_id');
    }

    public function User()
    {
        return $this->belongsTo(User::Class, 'user_id');
    }

    public function Room()
    {
        return $this->belongsTo(Rooms::Class, 'rooms_id');
    }

    public function Position()
    {
        return $this->belongsTo(PositionReservs::Class, 'position_reservs_id');
    }

    public function RoomsReserv()
    {
        return $this->hasMany(RoomReservs::class, 'reservs_id')->with('room');
    }

    protected function routeNotificationForLine()
    {
        return 'FQrUgIoZqSgWruTPCrI9iJVM72IchWPoiolt5kyZjqN';
    }
}
