<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $guarded = ['id'];
    protected $table = "roles";
    protected $fillable = [
        'name', 'label', 'description'
    ];
   
}
