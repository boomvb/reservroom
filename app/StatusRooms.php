<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusRooms extends Model
{
    protected $table = "status_rooms";
}
